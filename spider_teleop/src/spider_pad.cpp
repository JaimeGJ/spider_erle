#include <spider_teleop/spider_pad.h>

SpiderPad::SpiderPad():
    linear_x_(1),
    linear_y_(0),
    angular_(2),
    linear_z_(3)
  {
      current_vel = 0.1;
      gait_msg.data = 0;
      height = 1;

      //JOYSTICK PAD TYPE
      nh_.param<std::string>("pad_type",pad_type_,"ps3");
      //
      nh_.param("num_of_buttons", num_of_buttons_, DEFAULT_NUM_OF_BUTTONS);
      // MOTION CONF
      nh_.param("axis_linear_x", linear_x_, DEFAULT_AXIS_LINEAR_X);
      nh_.param("axis_linear_y", linear_y_, DEFAULT_AXIS_LINEAR_Y);
      nh_.param("axis_linear_z", linear_z_, DEFAULT_AXIS_LINEAR_Z);
      nh_.param("axis_angular", angular_, DEFAULT_AXIS_ANGULAR);
      nh_.param("scale_angular", a_scale_, DEFAULT_SCALE_ANGULAR);
      nh_.param("scale_linear", l_scale_, DEFAULT_SCALE_LINEAR);
      nh_.param("scale_linear_z", l_scale_z_, DEFAULT_SCALE_LINEAR_Z);
      nh_.param("cmd_topic_vel", cmd_topic_vel_, cmd_topic_vel_);
      nh_.param("cmd_gait_mode", cmd_gait_mode, cmd_gait_mode);
      nh_.param("cmd_height", cmd_height, cmd_height);
      nh_.param("button_height_up", button_height_up, button_height_up);
      nh_.param("button_height_down", button_height_down, button_height_down);
      nh_.param("button_dead_man", dead_man_button_, dead_man_button_);
      nh_.param("button_speed_up", speed_up_button_, speed_up_button_);  //4 Thrustmaster
      nh_.param("button_speed_down", speed_down_button_, speed_down_button_); //5 Thrustmaster
      nh_.param("button_gait_mode", button_gait_mode, button_gait_mode);
      nh_.param<std::string>("joystick_dead_zone", joystick_dead_zone_, "true");
      nh_.param("button_home", button_home_, DEFAULT_BUTTON_HOME);
      nh_.param("button_automatic", button_automatic, DEFAULT_BUTTON_AUTOMATIC);
      nh_.param("home_topic", home_topic_, home_topic_);
      // KINEMATIC MODE
      nh_.param("button_kinematic_mode", button_kinematic_mode_, button_kinematic_mode_);
      nh_.param("cmd_service_set_mode", cmd_set_mode_, cmd_set_mode_);
      nh_.param("cmd_service_home", cmd_home_, cmd_home_);
      kinematic_mode_ = 1;

      ROS_INFO("SummitXLPad num_of_buttons_ = %d", num_of_buttons_);
      for(int i = 0; i < num_of_buttons_; i++){
          bRegisteredButtonEvent[i] = false;
          ROS_INFO("bREG %d", i);
          }

      for(int i = 0; i < 3; i++){
        bRegisteredDirectionalArrows[i] = false;
      }

      /*ROS_INFO("Service I/O = [%s]", cmd_service_io_.c_str());
      ROS_INFO("Topic PTZ = [%s]", cmd_topic_ptz_.c_str());
      ROS_INFO("Service I/O = [%s]", cmd_topic_vel_.c_str());
      ROS_INFO("Axis linear = %d", linear_);
      ROS_INFO("Axis angular = %d", angular_);
      ROS_INFO("Scale angular = %d", a_scale_);
      ROS_INFO("Deadman button = %d", dead_man_button_);
      ROS_INFO("OUTPUT1 button %d", button_output_1_);
      ROS_INFO("OUTPUT2 button %d", button_output_2_);
      ROS_INFO("OUTPUT1 button %d", button_output_1_);
      ROS_INFO("OUTPUT2 button %d", button_output_2_);*/

      // Publish through the node handle Twist type messages to the guardian_controller/command topic
      vel_pub_ = nh_.advertise<geometry_msgs::Twist>(cmd_topic_vel_, 10);
      gait_pub = nh_.advertise<std_msgs::Int8>(cmd_gait_mode,10);
      //gait_crab_pub = nh_.advertise<crab_msgs::GaitCommand>("/teleop/gait_control",1);
      height_pub = nh_.advertise<std_msgs::Int8>(cmd_height,10);
      home_pub = nh_.advertise<std_msgs::Int8>(home_topic_,10); // Se llama home button, pero corresponde al select
      sweep = nh_.advertise<std_msgs::Int8>("/max_sweep", 10);

      // Listen through the node handle sensor_msgs::Joy messages from joystick
      // (these are the references that we will sent to summit_xl_controller/command)
      pad_sub_ = nh_.subscribe<sensor_msgs::Joy>("joy", 10, &SpiderPad::padCallback, this);
      pad_bridge_sub_ = nh_.subscribe<std_msgs::Bool>("pad_bridge", 10, &SpiderPad::padBridgeCallback, this);

      // Diagnostics
      updater_pad.setHardwareID("None");
      // Topics freq control
      min_freq_command = min_freq_joy = 10.0;
      max_freq_command = max_freq_joy = 100.0;
      sus_joy_freq = new diagnostic_updater::HeaderlessTopicDiagnostic("/joy", updater_pad,
                          diagnostic_updater::FrequencyStatusParam(&min_freq_joy, &max_freq_joy, 0.1, 10));

      pub_command_freq = new diagnostic_updater::HeaderlessTopicDiagnostic(cmd_topic_vel_.c_str(), updater_pad,
                          diagnostic_updater::FrequencyStatusParam(&min_freq_command, &max_freq_command, 0.1, 10));


      bEnable = false;	// Communication flag disabled by default
      last_command_ = true;
      Bridge_Enable = false;
  }


std_msgs::Int8 max_sweep;

  /*
   *	\brief Updates the diagnostic component. Diagnostics
   *
   */

 void SpiderPad::padBridgeCallback(const std_msgs::Bool::ConstPtr& bridge_enable_msg){
   if (bridge_enable_msg->data == true){
     Bridge_Enable = true;
   }
   else {
     Bridge_Enable = false;
   }
   ROS_INFO("Bridge_Enable is: %d ", Bridge_Enable);

 }


  void SpiderPad::Update(){
      updater_pad.update();
  }



  void SpiderPad::padCallback(const sensor_msgs::Joy::ConstPtr& joy)
  {
    if (joy->buttons[button_automatic] == 1){
      if(!bRegisteredButtonEvent[button_automatic]){
        Bridge_Enable = !Bridge_Enable;
        ROS_INFO("Bridge_Enable is: %d ", Bridge_Enable);
        bRegisteredButtonEvent[button_automatic] = true;
      }
    }
    else {
      bRegisteredButtonEvent[button_automatic] = false;
    }

    geometry_msgs::Twist vel;

    vel.linear.x = 0.0;
    vel.linear.y = 0.0;
    vel.linear.z = 0.0;

    vel.angular.x = 0.0;
    vel.angular.y = 0.0;
    vel.angular.z = 0.0;


    if(Bridge_Enable == false){
    bEnable = (joy->buttons[dead_man_button_] == 1);

    // Actions dependant on dead-man button
    if (joy->buttons[dead_man_button_] == 1) {

      //ROS_ERROR("SummitXLPad::padCallback: DEADMAN button %d", dead_man_button_);

      if ( joy->buttons[button_gait_mode] == 1 ){

        if(!bRegisteredButtonEvent[button_gait_mode]){
          if(gait_msg.data >= 3){
            gait_msg.data = 0;
            bRegisteredButtonEvent[button_gait_mode] = true;
            ROS_INFO("Gait Mode: %d", gait_msg.data);
            // sc.say(buf);
          }
          else {
            gait_msg.data += 1;
            bRegisteredButtonEvent[button_gait_mode] = true;
            ROS_INFO("Gait Mode: %d", gait_msg.data);
          }

        }
      }else{
        bRegisteredButtonEvent[button_gait_mode] = false;
        //Set the current velocity level
        if ( joy->buttons[speed_down_button_] == 1 ){

          if(!bRegisteredButtonEvent[speed_down_button_])
            if(current_vel > 0.1){
              current_vel = current_vel - 0.1;
              bRegisteredButtonEvent[speed_down_button_] = true;
              ROS_INFO("Velocity: %f%%", current_vel*100.0);
              char buf[50]="\0";
              int percent = (int) (current_vel*100.0);
              sprintf(buf," %d percent", percent);
              // sc.say(buf);
            }
        }else{
          bRegisteredButtonEvent[speed_down_button_] = false;
        }
        //ROS_ERROR("SummitXLPad::padCallback: Passed SPEED DOWN button %d", speed_down_button_);
        if (joy->buttons[speed_up_button_] == 1){
          if(!bRegisteredButtonEvent[speed_up_button_])
            if(current_vel < 0.9){
              current_vel = current_vel + 0.1;
              bRegisteredButtonEvent[speed_up_button_] = true;
              ROS_INFO("Velocity: %f%%", current_vel*100.0);
              char buf[50]="\0";
              int percent = (int) (current_vel*100.0);
              sprintf(buf," %d percent", percent);
              // sc.say(buf);
            }

        }else{
          bRegisteredButtonEvent[speed_up_button_] = false;
        }
        //ROS_ERROR("SummitXLPad::padCallback: Passed SPEED UP button %d", speed_up_button_);



        vel.linear.x = current_vel*l_scale_*joy->axes[linear_x_];
        vel.linear.y = current_vel*l_scale_*joy->axes[linear_y_];

        //ROS_ERROR("SummitXLPad::padCallback: Passed linear axes");

        if(joystick_dead_zone_=="true")
        {
          // limit scissor movement or robot turning (they are in the same joystick)
          if(joy->axes[angular_] == 1.0 || joy->axes[angular_] == -1.0) // if robot turning
          {
            // Same angular velocity for the three axis
            vel.angular.x = current_vel*(a_scale_*joy->axes[angular_]);
            vel.angular.y = current_vel*(a_scale_*joy->axes[angular_]);
            vel.angular.z = current_vel*(a_scale_*joy->axes[angular_]);

            vel.linear.z = 0.0;
          }
          else if (joy->axes[linear_z_] == 1.0 || joy->axes[linear_z_] == -1.0) // if scissor moving
          {
            vel.linear.z = current_vel*l_scale_z_*joy->axes[linear_z_]; // scissor movement

            // limit robot turn
            vel.angular.x = 0.0;
            vel.angular.y = 0.0;
            vel.angular.z = 0.0;
          }
          else
          {
            // Same angular velocity for the three axis
            vel.angular.x = current_vel*(a_scale_*joy->axes[angular_]);
            vel.angular.y = current_vel*(a_scale_*joy->axes[angular_]);
            vel.angular.z = current_vel*(a_scale_*joy->axes[angular_]);
            vel.linear.z = current_vel*l_scale_z_*joy->axes[linear_z_]; // scissor movement
          }
        }
        else // no dead zone
        {
          vel.angular.x = current_vel*(a_scale_*joy->axes[angular_]);
          vel.angular.y = current_vel*(a_scale_*joy->axes[angular_]);
          vel.angular.z = current_vel*(a_scale_*joy->axes[angular_]);
          vel.linear.z = current_vel*l_scale_z_*joy->axes[linear_z_];
        }

        //ROS_ERROR("SummitXLPad::padCallback: Passed joystick deadzone ifelse");

        if (joy->buttons[button_height_up] == 1) {

          if(!bRegisteredButtonEvent[button_height_up]){
            // Define mode (inc) - still coupled
            height += 1;
            if (height >= 3) height = 3;
            ROS_INFO("Leg Height: Profile %d ", height);
            bRegisteredButtonEvent[button_height_up] = true;
          }
        }else{
          bRegisteredButtonEvent[button_height_up] = false;
        }

        if (joy->buttons[button_height_down] == 1) {

          if(!bRegisteredButtonEvent[button_height_down]){
            // Define mode (dec) - still coupled
            height -= 1;
            if (height <= 1) height = 1;
            ROS_INFO("Leg Height: Profile %d ", height);
            bRegisteredButtonEvent[button_height_down] = true;
          }
        }else{
          bRegisteredButtonEvent[button_height_down] = false;
        }

        if (joy->buttons[button_kinematic_mode_] == 1) {

          if(!bRegisteredButtonEvent[button_kinematic_mode_]){
            // Define mode (inc) - still coupled
            kinematic_mode_ += 1;
            if (kinematic_mode_ > 2) kinematic_mode_ = 1;
            ROS_INFO("SummitXLJoy::joyCallback: Kinematic Mode %d ", kinematic_mode_);
            // Call service
            //robotnik_msgs::set_mode set_mode_srv;
            //set_mode_srv.request.mode = kinematic_mode_;
            //setKinematicMode.call( set_mode_srv );
            bRegisteredButtonEvent[button_kinematic_mode_] = true;
          }
        }else{
          bRegisteredButtonEvent[button_kinematic_mode_] = false;
        }

        //ROS_ERROR("SummitXLPad::padCallback: Passed SPHERE CAM and KINEMATIC MODE");

        if (joy->buttons[button_home_] == 1){
          if(!bRegisteredButtonEvent[button_home_]){
            home_ = 1;
            home_msg.data = home_;
            bRegisteredButtonEvent[button_home_] = true;
            max_sweep.data = 0;
            sweep.publish(max_sweep);
            ROS_INFO("Home button: %d", home_msg.data);
          }
        }
        else {
          home_ = 0;
          home_msg.data = home_;
          bRegisteredButtonEvent[button_home_] = false;
        }

      }
    }
    else {
      vel.angular.x = 0.0;	vel.angular.y = 0.0; vel.angular.z = 0.0;
      vel.linear.x = 0.0; vel.linear.y = 0.0; vel.linear.z = 0.0;
    }

    sus_joy_freq->tick();	// Ticks the reception of joy events

    // Publish
    // Only publishes if it's enabled
    if(bEnable){
      vel_pub_.publish(vel);
      gait_pub.publish(gait_msg);
      height_msg.data = height;
      height_pub.publish(height_msg);
      //home_msg.data = home_;
      home_pub.publish(home_msg);
      pub_command_freq->tick();
      last_command_ = true;
    }


    if(!bEnable && last_command_){

      vel.angular.x = 0.0;  vel.angular.y = 0.0; vel.angular.z = 0.0;
      vel.linear.x = 0.0;   vel.linear.y = 0.0; vel.linear.z = 0.0;
      vel_pub_.publish(vel);
      gait_pub.publish(gait_msg);
      height_msg.data = height;
      height_pub.publish(height_msg);
      //home_msg.data = home_;
      home_pub.publish(home_msg);
      pub_command_freq->tick();
      last_command_ = false;
    }
  }
  }


  int main(int argc, char** argv)
  {
      ros::init(argc, argv, "summit_xl_pad");
      SpiderPad my_spider_pad;

      ros::Rate r(10.0);

      while( ros::ok() )
      {
          // UPDATING DIAGNOSTICS
          my_spider_pad.Update();
          ros::spinOnce();
          r.sleep();
      }
  }

