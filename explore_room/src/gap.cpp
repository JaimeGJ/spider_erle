﻿#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <math.h>

//#include <string.h>
#include <iostream>
#include <unistd.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <thread>
#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <log4cxx/logger.h>

#define PI 3.14159265359;
#define COLOR_NORMAL "\033[0m"
#define COLOR_RED "\033[31m"
#define COLOR_GREEN "\033[32m"
#define COLOR_YELLOW "\033[33m"

//sensor_msgs::LaserScan laser;
int scan_state = 0;
#define n_superficies = 10;
int superficie[20];
float distancia[20];
int j=0;
int w=0;
float y;
float x;
float anterior, actual, longitud, alfa, hueco;
float min_dist1 = 100;
float min_dist2 = 100;
int min_sup1, min_sup2;
int sup_total = 0;
nav_msgs::Odometry spider_odom;
geometry_msgs::Twist vel;
std_msgs::Int8 gait;
std_msgs::Int8 height;
std_msgs::Bool pad_bridge_msg;
std_msgs::Int8 home;
sensor_msgs::Joy joy_msgs;

bool flag = false;
int recto, derecha, izquierda;

float abso (float n){
  float num = n;
  if (num < 0.0){
    num = -num;
  }
  return num;

}

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg){
  spider_odom.pose.pose.position.x = msg->pose.pose.position.x;
  spider_odom.pose.pose.position.y = msg->pose.pose.position.y;
  if (msg->pose.pose.orientation.z < 0.0){
    spider_odom.pose.pose.orientation.z = PI - msg->pose.pose.orientation.z;
  }
  else {
    spider_odom.pose.pose.orientation.z = msg->pose.pose.orientation.z ;
  }
}


int scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  if (flag == false){
    flag = true;
    float longitud = (msg->angle_max - msg->angle_min)/msg->angle_increment;
    float laser_filter[int(longitud)];
    for(int i = 0; i<= int(longitud) -1; i++){
      laser_filter[i] = 0;
    }
    for(int i = 0; i<= int(longitud) -1; i++){
      if ((msg->ranges[i] <= 100.0) && (msg->ranges[i] >= 0.0)){
        laser_filter[w] = msg->ranges[i];
        w++;
      }
    }
    for(int i = 1; i<= int(longitud)-1; i++){
      if (laser_filter[i] != 0){
        anterior = laser_filter[i-1];
        actual = laser_filter[i];
       // printf("%f \n", actual-anterior);
        if (abso(actual-anterior) >= 0.3){
          //if (longitud >= 0.01 && longitud <= 100.0){
            //ROS_INFO("Estado = he detectado un cambio");
            //printf("superficie[%d]: mide %f  y esta a %f \n", j, longitud, distancia[j]/superficie[j]);
            //if (longitud >= 0.5) {
            //  ROS_DEBUG("Puedo entrar en ese espacio");
           // }
            //printf("---------------------\n");
            distancia[j] = distancia[j]/superficie[j];
            j++;
            //printf("j: %d \n", j);
           // printf("actual: %f , anterior %f \n",actual, anterior);
          //}
        }
        else {
          //ROS_INFO("Estoy en otra superficie");
          superficie[j]++;
          distancia[j] = distancia[j] + actual;
        }
      }
      //printf("laser filtar %f \n", laser_filter[i]);
    }

    distancia[j] = distancia[j]/superficie[j];
     for(int i = 0; i<= j; i++){
       printf("superficie %d \n", superficie[i]);
       printf("distancia %f \n", distancia[i]);
     }
    //printf("seno %f \n", sin(3.14));

    float aux[20];

     for(int i = 0; i <= j; i++){
       aux[i] = distancia[i];
       if (aux[i] != 0){
         if (aux[i] < min_dist1){
           min_dist1 = aux[i];
           min_sup1 = i;

         }
       }
     }

     aux[min_sup1] = 0;

     for(int i = 0; i <= j; i++){
       if (aux[i] != 0){
         if (aux[i] < min_dist2){
           min_dist2 = aux[i];
           min_sup2 = i;
           aux[i] = 0;
         }
       }
     }

    printf("min_sup1: %d || min_sup2: %d \n", min_sup1, min_sup2);


     if (min_sup1 < min_sup2){              //dist1>dist2

       if (min_sup1 != 0){
         for(int i = 0; i <= min_sup1-1; i++){
            min_sup1 = min_sup1 + superficie[i];
           }
       }

       for(int i = min_sup1+1; i <= min_sup2-1; i++){
          sup_total = sup_total + superficie[i];
         }

       alfa = sup_total*msg->angle_increment;
       y = sin(alfa)*laser_filter[superficie[min_sup1]];
       x = laser_filter[int(superficie[min_sup1] + sup_total)] - (cos(alfa)*laser_filter[superficie[min_sup1]]);
       hueco = sqrt(x*x + y*y);
       printf("El hueco es de %f  y esta a %f \n", hueco, distancia[min_sup1+1]);
         if (hueco >= 0.5) {
           ROS_DEBUG("Puedo entrar en ese espacio");
         }
         else{
            ROS_ERROR("No puedo entrar en ese espacio");
         }
     }
     else {                                 //dist1<dist2
       if (min_sup1 != 0){
         for(int i = 0; i <= min_sup1-1; i++){
            min_sup1 = min_sup1 + superficie[i];
           }
       }
       for(int i = min_sup2; i <= min_sup1-1; i++){
          sup_total = sup_total + superficie[i];
       }
       alfa = sup_total*msg->angle_increment;
       y = sin(alfa)*laser_filter[superficie[min_sup1]];
       hueco = sqrt(x*x + y*y);
       printf("El hueco es de %f  y esta a %f \n", hueco, distancia[min_sup1+1]);
         if (hueco >= 0.5) {
           ROS_DEBUG("Puedo entrar en ese espacio");
         }
         else{
            ROS_ERROR("No puedo entrar en ese espacio");
         }
     }






   // y = sin(superficie[j]*msg->angle_increment)*laser_filter[i-1-superficie[j]];
   // x = anterior - (cos(superficie[j]*msg->angle_increment)*laser_filter[i-1-superficie[j]]);
   // longitud = sqrt(x*x + y*y);


    ROS_WARN("He acabado una iteracion");
    j=0;
    w=0;
  }

}


int main (int argc, char ** argv){
  // This needs to happen before we start fooling around with logger levels.  Otherwise the level we set may be overwritten by
  // a configuration file
  ROSCONSOLE_AUTOINIT;
  log4cxx::LoggerPtr my_logger = log4cxx::Logger::getLogger(ROSCONSOLE_DEFAULT_NAME);
  // Set the logger for this package to output all statements
  my_logger->setLevel(ros::console::g_level_lookup[ros::console::levels::Debug]);

  ros::init(argc, argv, "gap");
  ROS_INFO("Iniciando nodo 'gap' ");
  ros::NodeHandle n;

  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("/pad_teleop/cmd_vel", 1);
  ros::Publisher gait_pub = n.advertise<std_msgs::Int8>("/pad_teleop/gait_mode",1);
  ros::Publisher height_pub = n.advertise<std_msgs::Int8>("/pad_teleop/height",1);
  ros::Publisher home_pub = n.advertise<std_msgs::Int8>("/pad_teleop/home",1); // Se llama home button, pero corresponde al select
  ros::Publisher pad_bridge = n.advertise<std_msgs::Bool>("/pad_bridge", 1);
  ros::Publisher sweep_pub = n.advertise<std_msgs::Int8>("/max_sweep", 1);
  ros::Publisher joy_pub = n.advertise<sensor_msgs::Joy>("/joy", 1);
  ros::Subscriber odom_sub = n.subscribe<nav_msgs::Odometry>("/odom", 1, odomCallback);
  ros::Subscriber scan_sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 1, scanCallback);

  while(ros::ok()){

    //vel.linear.x = 1.0;
   // vel_pub.publish(vel);
    ros::spin();
    ros::Duration(1.0).sleep();
  }
 return 0;

}


/*
 * ---
header:
  seq: 22
  stamp:
    secs: 1532028655
    nsecs: 434484236
  frame_id: "/camera_depth_frame"
angle_min: -0.472028046846
angle_max: 0.495576918125
angle_increment: 0.00151424878277
time_increment: 0.0
scan_time: 0.0329999998212
range_min: 0.449999988079
range_max: 10.0
ranges: [640 medidas]
*/
