#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <math.h>

//#include <string.h>
#include <iostream>
#include <unistd.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>

int estado = 0;
int contador = 0;
int cont = 1; // Debe empezar en 1 para medir bien el ángulo que ha girado
int arriba = 0;
int abajo = 0;
int girado = 0;
std::string escaneo = "n";
std::string reescaneo = "a";

nav_msgs::Odometry spider_odom;
geometry_msgs::Twist vel;
std_msgs::Int8 gait;
std_msgs::Int8 height;
std_msgs::Bool pad_bridge_msg;
std_msgs::Int8 home;
sensor_msgs::Joy joy_msgs;

int max_sweep;
bool up_scan = false;
bool down_scan = false;
#define PI 3.14159265359

using namespace std;

void sweepCallback(const std_msgs::Int8::ConstPtr& msg){

  max_sweep = msg->data;

}

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg){
  /*spider_odom.pose.pose.position.x = msg->pose.pose.position.x;
  spider_odom.pose.pose.position.y = msg->pose.pose.position.y;
  spider_odom.pose.pose.position.z = msg->pose.pose.position.z;
  spider_odom.pose.pose.orientation.x = msg->pose.pose.orientation.x ;
  spider_odom.pose.pose.orientation.y = msg->pose.pose.orientation.y ;*/

  if (msg->pose.pose.orientation.z < 0.0){
    spider_odom.pose.pose.orientation.z = PI - msg->pose.pose.orientation.z*PI;
  }
  else {
    spider_odom.pose.pose.orientation.z = msg->pose.pose.orientation.z*PI ;
  }
}


int main (int argc, char ** argv){
  ros::init(argc, argv, "scanning");
  ROS_INFO("Iniciando nodo 'scanning' ");
  ros::NodeHandle n;

  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("/pad_teleop/cmd_vel", 1);
  ros::Publisher gait_pub = n.advertise<std_msgs::Int8>("/pad_teleop/gait_mode",1);
  ros::Publisher height_pub = n.advertise<std_msgs::Int8>("/pad_teleop/height",1);
  ros::Publisher home_pub = n.advertise<std_msgs::Int8>("/pad_teleop/home",1); // Se llama home button, pero corresponde al select
  ros::Publisher pad_bridge = n.advertise<std_msgs::Bool>("/pad_bridge", 1);
  ros::Publisher sweep_pub = n.advertise<std_msgs::Int8>("/max_sweep", 1);
  ros::Publisher joy_pub = n.advertise<sensor_msgs::Joy>("/joy", 1);
  ros::Subscriber sweep_sub = n.subscribe<std_msgs::Int8>("/max_sweep", 1, sweepCallback);
  ros::Subscriber odom_sub = n.subscribe<nav_msgs::Odometry>("/odom", 1, odomCallback);


  while(ros::ok()){
// ---------- ESTADO 0 ----------------------------//
    if (!estado){             // ESTADO 0 es esperar la confirmación
      if (escaneo == "s"){
        ROS_INFO("Iniciando escaneo de la habitacion...");
        gait.data = 3;
        gait_pub.publish(gait);
        pad_bridge_msg.data = true;
        pad_bridge.publish(pad_bridge_msg);
        estado = 1;

      }
      else if(escaneo == "n") {
        std::cout << "¿Iniciar escaneo? (s/n): ";
        std::cin >> escaneo;
      }
    }

// ---------- ESTADO 1 ----------------------------//
    else if (estado == 1) {   // ESTADO 1 es barrido hacia arriba
      ROS_INFO("Sweep: %d ", max_sweep);
      if (max_sweep){ //SI HA LLEGADO A LA POSICIÓN MÁS ABAJO
        //contador = 0;
        up_scan = true;
        ROS_INFO("Escaneo hacia abajo completado %d ", up_scan);
        estado = 3; // Es como pulsar el botón de home
      }
      else {
        ROS_INFO("Escaneo hacia arriba...");
        vel.linear.x = -0.1;
        vel.linear.y = 0.0;
        vel.linear.z = -0.1;
        vel.angular.x = 0.0;
        vel.angular.y = 0.0;
        vel.angular.z = 0.0;
        /*joy_msgs.axes[-0.0, -0.0, 0.0, -0.0, -0.0, 0.0, 0.0, 0.0]
        buttons: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]*/

      }

    }


// ---------- ESTADO 2 ----------------------------//
    else if (estado == 2){    // ESTADO 2 es barrido hacia abajo
      if (max_sweep){
        //contador = 0;
        down_scan = true;
        estado = 3;  // Es como pulsar el botón de home
      }
      else {
        ROS_INFO("Escaneo hacia abajo...");
        vel.linear.x = 0.1;
        vel.linear.y = 0.0;
        vel.linear.z = 0.1;
        vel.angular.x = 0.0;
        vel.angular.y = 0.0;
        vel.angular.z = 0.0;
        //contador++;
      }

    }

// ---------- ESTADO 3 ----------------------------//
    else if (estado == 3){    // ESTADO 3 es recuperar posición por defecto
      ROS_INFO("Recuperando posicion por defecto...");
      home.data = 1;
      home_pub.publish(home);
      ros::Duration(2.0).sleep(); // sleep for half a second
      home.data = 0;
      home_pub.publish(home);
      //if (up_scan == false) {
      //  estado = 1;
      //}else if (down_scan == false) {
      if (down_scan == false){
        estado = 2;
      }else if (up_scan && down_scan) {
        gait.data = 1;
        gait_pub.publish(gait);
        estado = 4;
      }
      vel.linear.x = 0.0;
      vel.linear.y = 0.0;
      vel.linear.z = 0.0;
      vel.angular.x = 0.0;
      vel.angular.y = 0.0;
      vel.angular.z = 0.0;

    }

// ---------- ESTADO 4 ----------------------------//
    else if (estado == 4){    // ESTADO 4 es giro en z

      if (abs(spider_odom.pose.pose.orientation.z - cont*0.7) <= 0.05){

        cont++;

        if (cont <= 9){ //360/40 = 9 veces las que ha de girar, 0.7 radianes cada vez
          up_scan = false;
          down_scan = false;
          estado = 1;
          gait.data = 3;
          gait_pub.publish(gait);
          contador = 0;
        }
        else {
          estado = 5;
        }

      }
      else {
        ROS_INFO("Rotando...");
        vel.linear.x = 0.0;
        vel.linear.y = 0.0;
        vel.linear.z = 0.0;
        vel.angular.x = 0.5;
        vel.angular.y = 0.5;
        vel.angular.z = 0.5;
        //contador++;
      }

    }

// ---------- ESTADO 5 ----------------------------//
    else {                    // ESTADO 5 es evaluación de resultados: otro escaneo o salir
      gait.data = 0;
      gait_pub.publish(gait);
      pad_bridge_msg.data = false;
      pad_bridge.publish(pad_bridge_msg);
      ROS_INFO("HE ACABADO EL ESCANEO");
      std::cout << "¿Se necesita otro? (s/n): ";
      std::cin  >> reescaneo;

      if (reescaneo == "s"){
        ROS_INFO("De acuerdo, volvemos a escanear");
        estado = 0;
        contador = 0;
        escaneo = "n";
        reescaneo = "a";
      }
      else if (reescaneo == "n") {
        ROS_INFO("De acuerdo");
        vel.linear.x = 0.0;
        vel.linear.y = 0.0;
        vel.linear.z = 0.0;
        vel.angular.x = 0.0;
        vel.angular.y = 0.0;
        vel.angular.z = 0.0;
        vel_pub.publish(vel);
        break;
      }


    }

    vel_pub.publish(vel);
    ros::spinOnce();
    ros::Duration(0.5).sleep(); // sleep for half a second
  }

return 0;
}


/*
 *
/joy
/pad_teleop/cmd_vel
/pad_teleop/gait_mode
/pad_teleop/height

 * */
