#include <ros/ros.h>
#include <ros/console.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <math.h>

//#include <string.h>
#include <iostream>
#include <unistd.h>
#include <std_msgs/Int8.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/LaserScan.h>
#include <thread>
#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <log4cxx/logger.h>


#define PI 3.14159265359

float media = 0;
float distancia = 5;
int length = 0;
nav_msgs::Odometry spider_odom;
geometry_msgs::Twist vel;
std_msgs::Int8 gait;
std_msgs::Int8 height;
std_msgs::Bool pad_bridge_msg;
std_msgs::Int8 home;
sensor_msgs::Joy joy_msgs;

bool flag = false;
bool end = true;
int estado = 0;
int recto = 0;
int derecha = 0;
int izquierda = 0;
float giro_actual;
int k_i = 1;
int k_d = 0;

void odomCallback(const nav_msgs::Odometry::ConstPtr& msg){
  spider_odom.pose.pose.position.x = msg->pose.pose.position.x;
  spider_odom.pose.pose.position.y = msg->pose.pose.position.y;
  //if (msg->pose.pose.orientation.z < 0.0){
  //  spider_odom.pose.pose.orientation.z = PI - msg->pose.pose.orientation.z;
  //}
  //else {
    spider_odom.pose.pose.orientation.z = msg->pose.pose.orientation.z*PI ;
  //}
}

float abso (float n){
  float num = n;
  if (num < 0.0){
    num = -num;
  }
  return num;

}
void scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
  if (flag == true){
    end = false;
    flag = false;
    media = 0;
    length = (msg->angle_max - msg->angle_min)/msg->angle_increment;
    for(int i = int(round(length/3)); i<= int(round(2*length/3))-1; i++){
      if ((msg->ranges[i] <= msg->range_max) && (msg->ranges[i] >= msg->range_min)){
        media = media + msg->ranges[i];
      }
    }
    ROS_INFO("Medidas tomadas");
    distancia = media/(length/3);
    end = true;
  }
}


int main (int argc, char ** argv){
  // This needs to happen before we start fooling around with logger levels.  Otherwise the level we set may be overwritten by
  // a configuration file
  ROSCONSOLE_AUTOINIT;
  log4cxx::LoggerPtr my_logger = log4cxx::Logger::getLogger(ROSCONSOLE_DEFAULT_NAME);
  // Set the logger for this package to output all statements
  my_logger->setLevel(ros::console::g_level_lookup[ros::console::levels::Debug]);

  ros::init(argc, argv, "navigation");
  ROS_INFO("Iniciando nodo 'navigation' ");
  ros::NodeHandle n;

  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("/pad_teleop/cmd_vel", 1);
  ros::Publisher gait_pub = n.advertise<std_msgs::Int8>("/pad_teleop/gait_mode",1);
  ros::Publisher height_pub = n.advertise<std_msgs::Int8>("/pad_teleop/height",1);
  ros::Publisher home_pub = n.advertise<std_msgs::Int8>("/pad_teleop/home",1); // Se llama home button, pero corresponde al select
  ros::Publisher pad_bridge = n.advertise<std_msgs::Bool>("/pad_bridge", 1);
  ros::Publisher sweep_pub = n.advertise<std_msgs::Int8>("/max_sweep", 1);
  ros::Publisher joy_pub = n.advertise<sensor_msgs::Joy>("/joy", 1);
  ros::Subscriber odom_sub = n.subscribe<nav_msgs::Odometry>("/odom", 10, odomCallback);
  ros::Subscriber scan_sub = n.subscribe<sensor_msgs::LaserScan>("/scan", 10, scanCallback);


/*
 * Estados:
 * 0 = avanzar recto
 * 1 = buscando salida
 * 2 = pararse
*/

  while(ros::ok()){
    ros::Duration(1.0).sleep();
    if (estado == 0) {  // Estado 0 es avanzar recto
      flag = true;
      while (end == false){} /*************************************/
      if (distancia >= 0.6 && distancia != 0){
        vel.linear.x = 1.0;
        vel.angular.x = 0.0;
        vel.angular.y = 0.0;
        vel.angular.z = 0.0;
        ROS_DEBUG("Distancia es: %0.3f . Puedo seguir andando recto", distancia);
      }
      else {
        estado = 1;
        giro_actual = spider_odom.pose.pose.orientation.z;
        printf("giro actual: %f \n", giro_actual);
        ROS_WARN("He de girar a la izquierda");
      }
    }
    else if (estado == 1){
      if (izquierda == 0){ //si aun no has visto la izquierda
        printf("giro_actual = %f \n", giro_actual);
        printf("se le resta = %f \n", k_i*(PI/6)+0.097);
        printf("odom = %f \n", spider_odom.pose.pose.orientation.z);
        printf("resultado = %f \n", abso(abso(giro_actual + k_i*(PI/6)+0.097) -  abso(spider_odom.pose.pose.orientation.z)));
        if (abso(abso(giro_actual + k_i*(PI/6)+0.097) -  abso(spider_odom.pose.pose.orientation.z)) <= 0.05){
          ROS_INFO("Comprobando distancia...");
          vel.linear.x = 0.0;
          vel.angular.x = 0.0;
          vel.angular.y = 0.0;
          vel.angular.z = 0.0;
          vel_pub.publish(vel);
          flag = true;
          while (end == false){} /*************************************/
          if (distancia >= 1.0 && distancia != 0){
            estado = 0;
            izquierda = 0;
            derecha = 0;
            k_d = 0;
            k_i = 1;
          }
          else{
            k_i++;
            if (k_i >= 4) {
              izquierda = 1;
              k_d = 0;
              ROS_WARN("He de girar a la derecha");
            }
          }
        }
        else {
          vel.linear.x = 0.0;
          vel.angular.x = 1.0;
          vel.angular.y = 1.0;
          vel.angular.z = 1.0;
          ROS_INFO("Girando a la izquierda");
        }
      }
      else{ // si ya has visto la izquierda
        k_i = 1;
        if (derecha == 0){ // si aun no has visto la derecha
          printf("giro_actual = %f \n", giro_actual);
          printf("se le resta = %f \n", k_d*(PI/6)+0.087);
          printf("odom = %f \n", spider_odom.pose.pose.orientation.z);
          printf("resultado = %f \n", abso(abso(giro_actual + k_d*(PI/6)) -  abso(spider_odom.pose.pose.orientation.z)));
          if (abso(abso(giro_actual - k_d*(PI/6)-0.087) -  abso(spider_odom.pose.pose.orientation.z)) <= 0.05){
            vel.linear.x = 0.0;
            vel.angular.x = 0.0;
            vel.angular.y = 0.0;
            vel.angular.z = 0.0;
            vel_pub.publish(vel);
            flag = true;
            while (end == false){} /*************************************/
            if (distancia >= 1.0 && distancia != 0){
              estado = 0;
              izquierda = 0;
              derecha = 0;
              k_d = 0;
              k_i = 1;
            }
            else{
              k_d++;
              if (k_d >= 4) {
                derecha = 1;
                k_d = 0;
              }
            }
          }
          else{
            vel.linear.x = 0.0;
            vel.angular.x = -1.0;
            vel.angular.y = -1.0;
            vel.angular.z = -1.0;
            ROS_INFO("Girando a la derecha");
          }
        }
        else{ // si ya has visto la derecha
          estado = 2;
        }
      }
    }

    else if (estado == 2){
      vel.linear.x = 0.0;
      vel.angular.x = 0.0;
      vel.angular.y = 0.0;
      vel.angular.z = 0.0;
      ROS_ERROR("No puedo avanzar por este pasillo");
      izquierda = 0;
      derecha = 0;
    }
    vel_pub.publish (vel);
    ros::spinOnce();

  }



 return 0;
}


