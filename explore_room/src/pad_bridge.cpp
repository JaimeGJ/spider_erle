#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include <geometry_msgs/Twist.h>
#include <math.h>

//#include <string.h>
#include <iostream>
#include <unistd.h>
#include <std_msgs/Int8.h>
#include <std_msgs/Bool.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>






int main (int argc, char ** argv){
  //Escribir en /pad_bridge True para poder utilizar esto.
  ros::init(argc, argv, "pad_bridge");
  ROS_INFO("Iniciando nodo 'pad_bridge' ");
  ros::NodeHandle n;

  ros::Publisher bridge_pub = n.advertise<std_msgs::Bool>("/pad_bridge", 1);
  std_msgs::Bool bridge;

  while(ros::ok()){
    bridge.data = true;
    bridge_pub.publish(bridge);
  }

return 0;
}
