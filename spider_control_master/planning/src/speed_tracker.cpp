// ROS 
#include <ros/ros.h>

// Custom Callback Queue
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>

// Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>

//Subscriber crab_maestro_controller
#include <crab_msgs/LegsJointsState.h>
#include <crab_msgs/GaitCommand.h>

#include <math.h>

#include <map>

#include <algorithm>
#include <assert.h>
#include <gazebo/common/Events.hh>
#include <gazebo/physics/physics.hh>
#include <math.h>

#include <gazebo_msgs/ModelStates.h>



//This node calculates the speed of the base link directly from Gazebo


void speedCallback(const gazebo_msgs::ModelStates::ConstPtr & data, ros::Time * t1, geometry_msgs::Pose * pos, geometry_msgs::Pose * last_pos) {



	pos->position.x += data->pose[2].position.x - last_pos->position.x;
	pos->position.y += data->pose[2].position.y - last_pos->position.y;
	pos->position.z += data->pose[2].position.z - last_pos->position.z;

	*last_pos = data->pose[2]; 

	if((ros::Time::now() - *t1).toSec() >=1.0) {
	
	ROS_INFO("Velocity:\n x: %lf \n y: %lf \n z: %lf \n",pos->position.x / ((ros::Time::now() - *t1).toSec()), pos->position.y / ((ros::Time::now() - *t1).toSec()), pos->position.z / ((ros::Time::now() - *t1).toSec()));

	*t1 = ros::Time::now();
	pos->position.x = 0.0;
	pos->position.y = 0.0;
	pos->position.z = 0.0;
	

	}
		

}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "speed_publisher");

  ros::NodeHandle n;

  ros::Time t1;

  geometry_msgs::Pose position, last_pos;

  position.position.x = 0.0;
  position.position.y = 0.0;
  position.position.z = 0.0;

  last_pos.position.x = 0.0;
  last_pos.position.y = 0.0;
  last_pos.position.z = 0.0;

  t1 = ros::Time::now();

  ros::Subscriber sub_1 = n.subscribe<gazebo_msgs::ModelStates>("/gazebo/model_states", 1, boost::bind(&speedCallback, _1, &t1, &position, &last_pos));

  ros::spin();  

  return 0;
}
