#ifndef SPIDER_HPP
#define SPIDER_HPP


#include <ros/ros.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <geometry_msgs/Twist.h>

#include <math.h>
#include <gsl/gsl_integration.h>

#define PI 3.14159265359


class Spider {


	private:

		//Position vectors

		//Right set
		std::vector<double> x_r, y_r, z_r, r_r;
		//Left set
		std::vector<double> x_l, y_l, z_l, r_l;

		std::vector<geometry_msgs::Pose> waypoints;
		moveit_msgs::RobotTrajectory trajectory1, trajectory2, trajectory3, trajectory4, trajectory5, trajectory6;

		//Arc function parameters

		//Right set
		double w_r, d_r;
		//Left set
		double w_l, d_l;

		//Height
		double z0;

		//Auxiliar parameters
		bool ok_pose1,ok_pose2,ok_pose3,ok_pose4,ok_pose5,ok_pose6, success;
		int i_;

		//eef velocity
		double v_leg; //Real~=0.318 m/s

		//fi angle: determines Vx and Vy : 0 => full x axis movement / PI/2 => full y axis movement
		double fi_l;
		double fi_r;

		//Desired velocity

		//Low profile:    Max=0.13 m/s     Min=0.07 m/s    z = 0.02 m
		//Medium profile: Max=0.11 m/s     Min=0.03 m/s    z = 0.05 m
		//High profile:   Max=0.09 m/s     Min=0.02 m/s    z = 0.08 m

		double v_spider; //Linear speed desired
		double rot; // Rotation: -1 means full clock-wise rotation / 1 means full anti clock-wise rotation

		double v_left;  //Left set linear velocity
		double v_right; //Right set linear velocity
		double max_vel; //Max linear velocity
		double min_vel; //Min linear velocity
		double rot_max; //Max angular velocity
		double rot_min; //Min angular velocity
    double sweep;   // Orientation of the sweep

		double vx, vy, w; //Command
		double last_w;    //Last w command

	public:		
	
		Spider(int profile); //Constructor
		~Spider(){}; //Destructor

		std::vector<double> getXVectorLeft(){return x_l;}
		std::vector<double> getYVectorLeft(){return y_l;}
		std::vector<double> getZVectorLeft(){return z_l;}

		std::vector<double> getXVectorRight(){return x_r;}
		std::vector<double> getYVectorRight(){return y_r;}
		std::vector<double> getZVectorRight(){return z_r;}

		bool setGait(const geometry_msgs::Twist & command); //Sets linear and angular velocity for each set based on joystick command

		void setPoints(); //Sets position arrays por each set

		double getFiLeft() {return fi_l;}
		double getFiRight() {return fi_r;}
		double getDLeft() {return d_l;}
		double getDRight() {return d_r;}
		double getVLeft() {return v_left;}
		double getVRight() {return v_right;}
    double getSweep() {return sweep;}
		double getZ() {return z0;}

		int getProfile();

		void setDLeft(double d) {d_l=d;}
		void setDRight(double d) {d_r=d;}
		void setProfile(int p); //Sets leg height

		void setTrajectory1(moveit_msgs::RobotTrajectory trajectory) {trajectory1 = trajectory;}
		void setTrajectory2(moveit_msgs::RobotTrajectory trajectory) {trajectory2 = trajectory;}
		void setTrajectory3(moveit_msgs::RobotTrajectory trajectory) {trajectory3 = trajectory;}
		void setTrajectory4(moveit_msgs::RobotTrajectory trajectory) {trajectory4 = trajectory;}
		void setTrajectory5(moveit_msgs::RobotTrajectory trajectory) {trajectory5 = trajectory;}
		void setTrajectory6(moveit_msgs::RobotTrajectory trajectory) {trajectory6 = trajectory;}

		moveit_msgs::RobotTrajectory getTrajectory1() {return trajectory1;}
		moveit_msgs::RobotTrajectory getTrajectory2() {return trajectory2;}
		moveit_msgs::RobotTrajectory getTrajectory3() {return trajectory3;}
		moveit_msgs::RobotTrajectory getTrajectory4() {return trajectory4;}
		moveit_msgs::RobotTrajectory getTrajectory5() {return trajectory5;}
		moveit_msgs::RobotTrajectory getTrajectory6() {return trajectory6;}

};




#endif
