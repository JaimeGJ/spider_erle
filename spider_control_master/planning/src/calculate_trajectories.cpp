#include "calculate_trajectories.hpp"

//This file calculates the z and d parameters for the trajectory of each leg

//Arcs
double f_low (double t, void * params) {

	double alpha = * (double *) params;
	double z0 = 0.02;
	return sqrt(1 + pow(alpha * z0,2) * pow(cos(alpha*t),2));

}


double f_medium (double t, void * params) {

	double alpha = * (double *) params;
	double z0 = 0.05;
	return sqrt(1 + pow(alpha * z0,2) * pow(cos(alpha*t),2));

}


double f_high (double t, void * params) {

	double alpha = * (double *) params;
	double z0 = 0.08;
	return sqrt(1 + pow(alpha * z0,2) * pow(cos(alpha*t),2));

}


void calculateTrajectory (double v_spider, double z0, double * d, double v_leg) {

	double L;
	double error;
	double t1, t2, total_time;
	double spider_speed;
	double l = 0.1;
	double w;
	double diff;
	bool found = false;
	int z;

	//Calculate arc lenght
	gsl_integration_workspace * ws = gsl_integration_workspace_alloc (1000);


	if (z0 == 0.02)
		z = 1;

	else if(z0 == 0.05)
		z=2;

	else if(z0 == 0.08)
		z=3;
	else
		z=0;


	if(v_spider == 0.0) {

		*d = 0.0;
		found = true;
	}

	else {
	
		switch (z) {

		//Low profile


			case 1:
	

				for (l = 0.1; l>=0.01; l-=0.015) {

					w = PI / l;
					gsl_function F;
					F.function = &f_low;
					F.params = &w;

					gsl_integration_qags (&F, 0, PI/w, 0, 1e-7, 1000, ws, &L, &error);

					t1 = L / v_leg;
					t2 = (PI/w) / v_leg;
					total_time = t1+t2;


					spider_speed = l / (t1 + t2);
	
					diff = spider_speed - v_spider;

					if(diff < 0)
						diff = -diff;

					if (diff <= 0.015) {
						
						found = true;
						*d = l;

						ROS_INFO("Expected speed: %lf \n Parameters: z0: %lf \n d: %lf \n", spider_speed, z0, *d);
						break; 

					}
				}

			break;		

		
	
		//Medium profile

			case 2:

				for (l = 0.1; l>=0.01; l-=0.015) {

					w = PI / l;
					gsl_function F;
					F.function = &f_medium;
					F.params = &w;

					gsl_integration_qags (&F, 0, PI/w, 0, 1e-7, 1000, ws, &L, &error);

					t1 = L / v_leg;
					t2 = (PI/w) / v_leg;
					total_time = t1+t2;


					spider_speed = l / (t1 + t2);
					diff = spider_speed - v_spider;

					if(diff < 0)
						diff = -diff;	

					if (diff <= 0.015) {
					
						found = true;
						*d = l;

						ROS_INFO("Expected speed: %lf \n Parameters: z0: %lf \n d: %lf \n", spider_speed, z0, *d);
						break; 
					}

				}
			break;
					

		

		//High profile

			case 3:

				for (l = 0.1; l>=0.01; l-=0.015) {

					w = PI / l;
					gsl_function F;
					F.function = &f_high;
					F.params = &w;

					gsl_integration_qags (&F, 0, PI/w, 0, 1e-7, 1000, ws, &L, &error);

					t1 = L / v_leg;
					t2 = (PI/w) / v_leg;
					total_time = t1+t2;


					spider_speed = l / (t1 + t2);
					diff = spider_speed - v_spider;

					if(diff < 0)
						diff = -diff;

					if (diff <= 0.015) {
					
						found = true;
						*d = l;

						ROS_INFO("Expected speed: %lf \n Parameters: z0: %lf \n d: %lf \n", spider_speed, z0, *d);
						break; 

					}
				}

			break;

			default:
			break;

		}

	}
	

	if(!found) {

		ROS_FATAL("ERROR: Not trajectory found: Using default trajectory... \n");

		*d = 0.05;

	}
	
	gsl_integration_workspace_free (ws);


}

