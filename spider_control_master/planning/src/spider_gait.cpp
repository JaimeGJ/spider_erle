#include <ros/ros.h>
#include <ros/callback_queue.h>
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include "calculate_trajectories.cpp"
#include "spider.cpp"

#include <std_msgs/Int8.h>

#define PI 3.14159265359



void twistCallback(const geometry_msgs::Twist::ConstPtr & command, Spider * spider, bool * change, double * v_left, double * v_right, bool * tripod_flag) {

	Spider spider_aux = *spider;
	double d_r, d_l, z0;
	double v_leg = 0.3;

	*change = spider_aux.setGait(*command);

	if(*change) {

	*v_left = spider_aux.getVLeft();
	*v_right = spider_aux.getVRight();
	z0 = spider_aux.getZ();

	calculateTrajectory(2 * (*v_left), z0, &d_l, v_leg);
	calculateTrajectory(2 * (*v_right), z0, &d_r, v_leg);

	spider_aux.setDLeft(d_l);
	spider_aux.setDRight(d_r);

	ROS_INFO("d_l: %lf \n d_r: %lf \n ---------------", d_l, d_r);
	
	spider_aux.setPoints();

	*spider = spider_aux;
	*tripod_flag = true;

	}


}


void gaitCallback(const std_msgs::Int8::ConstPtr & msg, int * gait_mode) {


	*gait_mode = msg->data;

}

void heightCallback(const std_msgs::Int8::ConstPtr & msg, int * profile, Spider * spider, bool * tripod_flag) {


	Spider spider_aux = *spider;
	if (*profile != msg->data) {

		*profile = msg->data;
		spider_aux.setProfile(*profile);

		*tripod_flag = true;

		double v_left = spider_aux.getVLeft();
		double v_right = spider_aux.getVRight();
		double d_l, d_r;
		double v_leg = 0.3;
		double z0 = spider_aux.getZ();

		calculateTrajectory(2 * (v_left), z0, &d_l, v_leg);
		calculateTrajectory(2 * (v_right), z0, &d_r, v_leg);

		spider_aux.setDLeft(d_l);
		spider_aux.setDRight(d_r);
	
		spider_aux.setPoints();

		*spider = spider_aux;

	}

	

}

void homeCallback (const std_msgs::Int8::ConstPtr & msg, int * home){

  *home = msg->data;

}


int main(int argc, char **argv) {

ros::init(argc, argv, "spider_gait");
  ros::NodeHandle node_handle;
  ros::NodeHandle node_handle2;  

  ros::CallbackQueue my_queue;
  node_handle2.setCallbackQueue(&my_queue);
	
  ros::AsyncSpinner spinner(0);
  spinner.start();
  
 // ros::Rate r(10);

//Parameters
//Position vectors

//Right set
std::vector<double> x_r, y_r, z_r, r_r;
//Left set
std::vector<double> x_l, y_l, z_l, r_l;

moveit_msgs::RobotTrajectory trajectory1, trajectory2, trajectory3, trajectory4, trajectory5, trajectory6;

std::vector<geometry_msgs::Pose> waypoints;

moveit::planning_interface::MoveGroup::Plan my_plan;

double d_l, d_r;
double fi_l, fi_r;
double v_left, v_right;
double v_exec_l, v_exec_r;


int i = 0;
int i_ = 0;
int j_ = 1;
int home;
std_msgs::Int8 home_msg;
std_msgs::Int8 max_sweep;
geometry_msgs::Twist initial;
int gait_mode, old_gait_mode;
int profile, old_profile;
bool set1, set2, set3, change, tripod_flag, first;
bool ok_pose1,ok_pose2,ok_pose3,ok_pose4,ok_pose5,ok_pose6, success;

initial.linear.x = 0.0;
initial.linear.y = 0.0;
initial.linear.z = 0.0;

initial.angular.x = 0.0;
initial.angular.y = 0.0;
initial.angular.z = 0.0;

set1 = true;
set2 = false;
set3 = false;
change = false;
gait_mode = 0;
profile = 1;

//Setup

moveit::planning_interface::PlanningSceneInterface planning_scene_interface;

moveit::planning_interface::MoveGroup full_group("all_legs");
full_group.setPoseReferenceFrame("base_link");

moveit::planning_interface::MoveGroup group1("leg_l1"); //Left
moveit::planning_interface::MoveGroup group2("leg_l2"); //Left
moveit::planning_interface::MoveGroup group3("leg_l3"); //Left
moveit::planning_interface::MoveGroup group4("leg_r1"); //Right
moveit::planning_interface::MoveGroup group5("leg_r2"); //Right
moveit::planning_interface::MoveGroup group6("leg_r3"); //Right


group1.setPoseReferenceFrame("base_link");
group2.setPoseReferenceFrame("base_link");
group3.setPoseReferenceFrame("base_link");
group4.setPoseReferenceFrame("base_link");
group5.setPoseReferenceFrame("base_link");
group6.setPoseReferenceFrame("base_link");


group1.setPlanningTime(30);
group1.setGoalTolerance(0.005);

group2.setPlanningTime(30);
group2.setGoalTolerance(0.008);

group3.setPlanningTime(30);
group3.setGoalTolerance(0.008);

group4.setPlanningTime(30);
group4.setGoalTolerance(0.008);

group5.setPlanningTime(30);
group5.setGoalTolerance(0.008);

group6.setPlanningTime(30);
group6.setGoalTolerance(0.008);

full_group.setPlanningTime(30);
full_group.setGoalTolerance(0.008);

Spider spider(1);
spider.setGait(initial);


//Get current poses
geometry_msgs::PoseStamped target_pose_current_leg_l1;
geometry_msgs::PoseStamped target_pose_current_leg_r2;
geometry_msgs::PoseStamped target_pose_current_leg_l3;

geometry_msgs::PoseStamped target_pose_current_leg_r1;
geometry_msgs::PoseStamped target_pose_current_leg_l2;
geometry_msgs::PoseStamped target_pose_current_leg_r3;

geometry_msgs::PoseStamped target_pose_current;
geometry_msgs::Pose target_pose;

geometry_msgs::Pose target_pose1;
geometry_msgs::Pose target_pose2;
geometry_msgs::Pose target_pose3;

geometry_msgs::Pose target_pose4;
geometry_msgs::Pose target_pose5;
geometry_msgs::Pose target_pose6;


geometry_msgs::Pose start_pose1;
geometry_msgs::Pose start_pose2;
geometry_msgs::Pose start_pose3;

geometry_msgs::Pose start_pose4;
geometry_msgs::Pose start_pose5;
geometry_msgs::Pose start_pose6;

target_pose_current_leg_l1 = full_group.getCurrentPose("tibia_foot_l1");
target_pose_current_leg_r2 = full_group.getCurrentPose("tibia_foot_r2");
target_pose_current_leg_l3 = full_group.getCurrentPose("tibia_foot_l3");

target_pose_current_leg_r1 = full_group.getCurrentPose("tibia_foot_r1");
target_pose_current_leg_l2 = full_group.getCurrentPose("tibia_foot_l2");
target_pose_current_leg_r3 = full_group.getCurrentPose("tibia_foot_r3");

target_pose1 = target_pose_current_leg_l1.pose; //Left
target_pose2 = target_pose_current_leg_l2.pose; //Right
target_pose3 = target_pose_current_leg_l3.pose; //Left

target_pose4 = target_pose_current_leg_r1.pose; //Right
target_pose5 = target_pose_current_leg_r2.pose; //Left
target_pose6 = target_pose_current_leg_r3.pose; //Right

//Start state

std::vector<double> group_variable_values1;
std::vector<double> group_variable_values2;
std::vector<double> group_variable_values3;
std::vector<double> group_variable_values4;
std::vector<double> group_variable_values5;
std::vector<double> group_variable_values6;

std::vector<double> group_variable_values_full;

group1.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group1.getName()), group_variable_values1);
group2.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group2.getName()), group_variable_values2);
group3.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group3.getName()), group_variable_values3);
group4.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group4.getName()), group_variable_values4);
group5.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group5.getName()), group_variable_values5);
group6.getCurrentState()->copyJointGroupPositions(group1.getCurrentState()->getRobotModel()->getJointModelGroup(group6.getName()), group_variable_values6);
full_group.getCurrentState()->copyJointGroupPositions(full_group.getCurrentState()->getRobotModel()->getJointModelGroup(full_group.getName()), group_variable_values_full);


start_pose1 = target_pose1;
start_pose2 = target_pose2;
start_pose3 = target_pose3;

start_pose4 = target_pose4;
start_pose5 = target_pose5;
start_pose6 = target_pose6;

//Subscribe to topic

  ros::SubscribeOptions ops = ros::SubscribeOptions::create<geometry_msgs::Twist>("/pad_teleop/cmd_vel", 1, boost::bind(&twistCallback, _1, &spider, &change, &v_left, &v_right, &tripod_flag),ros::VoidPtr(),&my_queue);

  ros::Subscriber sub = node_handle2.subscribe(ops);

  ros::Subscriber sub_gait = node_handle2.subscribe<std_msgs::Int8>("/pad_teleop/gait_mode", 1, boost::bind(&gaitCallback, _1, &gait_mode));
  ros::Subscriber sub_height = node_handle2.subscribe<std_msgs::Int8>("/pad_teleop/height", 1, boost::bind(&heightCallback, _1, &profile, &spider, &tripod_flag));
  ros::Subscriber sub_home = node_handle2.subscribe<std_msgs::Int8>("/pad_teleop/home", 1, boost::bind(&homeCallback, _1, &home));
  ros::Publisher sweep = node_handle2.advertise<std_msgs::Int8>("/max_sweep", 1);
  ros::AsyncSpinner spinner2(1, &my_queue);
  spinner2.start();


while(node_handle.ok()) {


	if(change || (((v_exec_l!=spider.getVLeft())&&(v_exec_r!=spider.getVRight()) && !gait_mode)) || ((gait_mode != old_gait_mode) && gait_mode) || ((old_profile != spider.getProfile()) && !gait_mode) || (tripod_flag && gait_mode)) {

		//Return robot to initial pose
			
			i_=0;
			j_=1;
			tripod_flag = false;
			first = true;
	
			if(!gait_mode) {
			
				group1.setJointValueTarget(group_variable_values1);
				group2.setJointValueTarget(group_variable_values2);
				group3.setJointValueTarget(group_variable_values3);
				group4.setJointValueTarget(group_variable_values4);
				group5.setJointValueTarget(group_variable_values5);
				group6.setJointValueTarget(group_variable_values6);
			
				group1.move();
				group2.move();
				group3.move();
				group4.move();
				group5.move();
				group6.move();

			}

      else if(gait_mode != 3) {

				full_group.setJointValueTarget(group_variable_values_full);
				full_group.move();
			}

			target_pose_current_leg_l1 = full_group.getCurrentPose("tibia_foot_l1");
			target_pose_current_leg_r2 = full_group.getCurrentPose("tibia_foot_r2");
			target_pose_current_leg_l3 = full_group.getCurrentPose("tibia_foot_l3");

			target_pose_current_leg_r1 = full_group.getCurrentPose("tibia_foot_r1");
			target_pose_current_leg_l2 = full_group.getCurrentPose("tibia_foot_l2");
			target_pose_current_leg_r3 = full_group.getCurrentPose("tibia_foot_r3");

			target_pose1 = target_pose_current_leg_l1.pose; //Left
			target_pose2 = target_pose_current_leg_l2.pose; //Right
			target_pose3 = target_pose_current_leg_l3.pose; //Left

			target_pose4 = target_pose_current_leg_r1.pose; //Right
			target_pose5 = target_pose_current_leg_r2.pose; //Left
			target_pose6 = target_pose_current_leg_r3.pose; //Right

			//Set points

			x_l.clear();
			y_l.clear();
			z_l.clear();

			x_r.clear();
			y_r.clear();
			z_r.clear();

			r_l.clear();
			r_r.clear();


			if((spider.getVLeft() != 0.0) && (spider.getVRight() != 0.0)) {

				d_r = spider.getDRight();
				d_l = spider.getDLeft();
				fi_l = spider.getFiLeft();
				fi_r = spider.getFiRight();

				x_l = spider.getXVectorLeft();
				y_l = spider.getYVectorLeft();
				z_l = spider.getZVectorLeft();

				x_r = spider.getXVectorRight();
				y_r = spider.getYVectorRight();
				z_r = spider.getZVectorRight();

			}




		if(!gait_mode) {

			//Planning

			//Orden: l1,r2,l3,r1,l2,r3

			//---------------------------------------------------------Leg_l1 (1)-------------------------------------------------------//

			target_pose_current = group1.getCurrentPose("tibia_foot_l1");
			target_pose = target_pose_current.pose;

			group1.setStartState(*group1.getCurrentState());

			for (i = 1; i < x_l.size(); i++) {

				target_pose.position.x += x_l[i] - x_l[i-1];
				target_pose.position.y += y_l[i] - y_l[i-1];
				target_pose.position.z += z_l[i] - z_l[i-1];

				if(i == (x_l.size()-1)) {
					target_pose.position.z -=0.008;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_l * cos(fi_l);
			target_pose.position.y -= d_l * sin(fi_l);
			waypoints.push_back(target_pose);

			target_pose.position.z += 0.008;
			waypoints.push_back(target_pose);


			//Compute trajectory

			double fraction = group1.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory1); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);

			spider.setTrajectory1(trajectory1);

			//Clear trajectory
			waypoints.clear();

			//---------------------------------------------------------Leg_r2 (5)-------------------------------------------------------//

			target_pose_current = group5.getCurrentPose("tibia_foot_r2");
			target_pose = target_pose_current.pose;

			group5.setStartState(*group5.getCurrentState());

			for (i = 1; i < x_r.size(); i++) {

				target_pose.position.x += x_r[i] - x_r[i-1];
				target_pose.position.y += y_r[i] - y_r[i-1];
				target_pose.position.z += z_r[i] - z_r[i-1];

				if(i == (x_r.size()-1)) {
					target_pose.position.z -=0.003;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_r * cos(fi_r);
			target_pose.position.y -= d_r * sin(fi_r);

			waypoints.push_back(target_pose);

			target_pose.position.z += 0.003;
			waypoints.push_back(target_pose);


			//Compute trajectory

			fraction = group5.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory5); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);
		
			spider.setTrajectory5(trajectory5);

			//Clear trajectory
			waypoints.clear();

			//---------------------------------------------------------Leg_l3 (3)-------------------------------------------------------//

			target_pose_current = group3.getCurrentPose("tibia_foot_l3");
			target_pose = target_pose_current.pose;

			group3.setStartState(*group3.getCurrentState());

			for (i = 1; i < x_l.size(); i++) {

				target_pose.position.x += x_l[i] - x_l[i-1];
				target_pose.position.y += y_l[i] - y_l[i-1];
				target_pose.position.z += z_l[i] - z_l[i-1];

				if(i == (x_l.size()-1)) {
					target_pose.position.z -=0.003;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_l * cos(fi_l);
			target_pose.position.y -= d_l * sin(fi_l);

			waypoints.push_back(target_pose);

			target_pose.position.z += 0.003;
			waypoints.push_back(target_pose);

			//Compute trajectory

			fraction = group3.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory3); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);

			spider.setTrajectory3(trajectory3);

			//Clear trajectory
			waypoints.clear();

			//---------------------------------------------------------Leg_r1 (4)-------------------------------------------------------//

			target_pose_current = group4.getCurrentPose("tibia_foot_r1");
			target_pose = target_pose_current.pose;

			group4.setStartState(*group4.getCurrentState());

			for (i = 1; i < x_r.size(); i++) {

				target_pose.position.x += x_r[i] - x_r[i-1];
				target_pose.position.y += y_r[i] - y_r[i-1];
				target_pose.position.z += z_r[i] - z_r[i-1];

				if(i == (x_r.size()-1)) {
					target_pose.position.z -=0.004;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_r * cos(fi_r);
			target_pose.position.y -= d_r * sin(fi_r);

			waypoints.push_back(target_pose);

			target_pose.position.z += 0.004;
			waypoints.push_back(target_pose);

			//Compute trajectory

			fraction = group4.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory4); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);

			spider.setTrajectory4(trajectory4);

			//Clear trajectory
			waypoints.clear();

			//---------------------------------------------------------Leg_l2 (2)-------------------------------------------------------//

			target_pose_current = group2.getCurrentPose("tibia_foot_l2");
			target_pose = target_pose_current.pose;

			group2.setStartState(*group2.getCurrentState());

			for (i = 1; i < x_l.size(); i++) {

				target_pose.position.x += x_l[i] - x_l[i-1];
				target_pose.position.y += y_l[i] - y_l[i-1];
				target_pose.position.z += z_l[i] - z_l[i-1];

				if(i == (x_l.size()-1)) {
					target_pose.position.z -=0.003;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_l * cos(fi_l);
			target_pose.position.y -= d_l * sin(fi_l);

			waypoints.push_back(target_pose);

			target_pose.position.z += 0.003;
			waypoints.push_back(target_pose);

			//Compute trajectory

			fraction = group2.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory2); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);

			spider.setTrajectory2(trajectory2);

			//Clear trajectory
			waypoints.clear();

			//---------------------------------------------------------Leg_r3 (6)-------------------------------------------------------//

			target_pose_current = group6.getCurrentPose("tibia_foot_r3");
			target_pose = target_pose_current.pose;

			group6.setStartState(*group6.getCurrentState());

			for (i = 1; i < x_r.size(); i++) {

				target_pose.position.x += x_r[i] - x_r[i-1];
				target_pose.position.y += y_r[i] - y_r[i-1];
				target_pose.position.z += z_r[i] - z_r[i-1];

				if(i == (x_r.size()-1)) {
					target_pose.position.z -=0.003;
					ROS_INFO("Bajando punto... \n");
				}

				waypoints.push_back(target_pose);

			}

			//Return

			target_pose.position.x -= d_r * cos(fi_r);
			target_pose.position.y -= d_r * sin(fi_r);

			waypoints.push_back(target_pose);

			target_pose.position.z += 0.003;
			waypoints.push_back(target_pose);

			//Compute trajectory

			fraction = group6.computeCartesianPath(waypoints,	
								     0.01,  //step size
								     0.0,    //jump threshold
								     trajectory6); 

			ROS_INFO(" Cartesian path: (%.2f%% acheived) \n", fraction * 100.0);

			//Clear trajectory
			waypoints.clear();

			spider.setTrajectory6(trajectory6);

		


			change = false;

		}

	}



	if (!x_l.empty() && !x_r.empty()) {

	//---------------------------------------------WAVE GAIT------------------------------------------------------//

		if(!gait_mode) {

			switch(i_) {

				case 0 : 

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory1();
					group1.execute(my_plan);
					i_++;

				break;


				case 1 : 
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory5();
					group5.execute(my_plan);
					i_++;

				break;


				case 2 : 
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory3();
					group3.execute(my_plan);
					i_++;

				break;


				case 3 : 
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory4();
					group4.execute(my_plan);
					i_++;

				break;


				case 4 : 
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory2();
					group2.execute(my_plan);
					i_++;

				break;


				case 5 : 
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();
					my_plan.trajectory_ = spider.getTrajectory6();
					group6.execute(my_plan);
					i_= 0;


				break;

				default:
					i_= 0;

				break;	
			}

		}

	//-------------------------------------TRIPOD GAIT----------------------------------------------------//

		else if (gait_mode == 1) {

			if (set1 && !set2) {


				if (j_ != x_l.size()) {

					target_pose1.position.x += x_l[j_] - x_l[j_-1];
					target_pose1.position.y += y_l[j_] - y_l[j_-1];
					target_pose1.position.z += z_l[j_] - z_l[j_-1];

					target_pose5.position.x += x_r[j_] - x_r[j_-1];
					target_pose5.position.y += y_r[j_] - y_r[j_-1];
					target_pose5.position.z += z_r[j_] - z_r[j_-1];

					target_pose3.position.x += x_l[j_] - x_l[j_-1];
					target_pose3.position.y += y_l[j_] - y_l[j_-1];
					target_pose3.position.z += z_l[j_] - z_l[j_-1];

					if(j_ == (x_l.size()-1)) {
						target_pose1.position.z -=0.005;
						target_pose5.position.z -=0.005;
						target_pose3.position.z -=0.005;
						ROS_INFO("Bajando punto... \n");
					}


					if ((j_==1) && !first) {


						target_pose2.position.z +=0.005;
						target_pose4.position.z +=0.005;
						target_pose6.position.z +=0.005;


					}

					j_++;

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					full_group.move();
					
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

				}

				else {

					//Return

					target_pose1.position.x -= d_l * cos(fi_l);
					target_pose1.position.y -= d_l * sin(fi_l);


					target_pose5.position.x -= d_r * cos(fi_r);
					target_pose5.position.y -= d_r * sin(fi_r);


					target_pose3.position.x -= d_l * cos(fi_l);
					target_pose3.position.y -= d_l * sin(fi_l);


					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					j_=1;
					set1=false;
					set2=true;

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();


				}

			}

			if (!set1 && set2) {


				if (j_ != x_l.size()) {

					target_pose4.position.x += x_r[j_] - x_r[j_-1];
					target_pose4.position.y += y_r[j_] - y_r[j_-1];
					target_pose4.position.z += z_r[j_] - z_r[j_-1];

					target_pose2.position.x += x_l[j_] - x_l[j_-1];
					target_pose2.position.y += y_l[j_] - y_l[j_-1];
					target_pose2.position.z += z_l[j_] - z_l[j_-1];

					target_pose6.position.x += x_r[j_] - x_r[j_-1];
					target_pose6.position.y += y_r[j_] - y_r[j_-1];
					target_pose6.position.z += z_r[j_] - z_r[j_-1];

					if(j_ == (x_l.size()-1)) {
						target_pose4.position.z -=0.005;
						target_pose2.position.z -=0.005;
						target_pose6.position.z -=0.005;
						ROS_INFO("Bajando punto... \n");
					}

					if ((j_==1) && !first) {


						target_pose1.position.z +=0.005;
						target_pose3.position.z +=0.005;
						target_pose5.position.z +=0.005;


					}

					j_++;

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					full_group.move();

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

				}


				else {

					//Return

					target_pose4.position.x -= d_r * cos(fi_r);
					target_pose4.position.y -= d_r * sin(fi_r);


					target_pose2.position.x -= d_l * cos(fi_l);
					target_pose2.position.y -= d_l * sin(fi_l);


					target_pose6.position.x -= d_r * cos(fi_r);
					target_pose6.position.y -= d_r * sin(fi_r);


					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					j_=1;
					set1=true;
					set2=false;

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

					if (first) {first = false;}


				}
		
			}
			

		}

	//-----------------------------------------------RIPPLE GAIT----------------------------------------------------------------//

    else if (gait_mode == 2) {

			if (set1 && !set2 && !set3) {


				if (j_ != x_l.size()) {

					target_pose1.position.x += x_l[j_] - x_l[j_-1];
					target_pose1.position.y += y_l[j_] - y_l[j_-1];
					target_pose1.position.z += z_l[j_] - z_l[j_-1];

					target_pose6.position.x += x_r[j_] - x_r[j_-1];
					target_pose6.position.y += y_r[j_] - y_r[j_-1];
					target_pose6.position.z += z_r[j_] - z_r[j_-1];

					if(j_ == (x_l.size()-1)) {
						target_pose1.position.z -=0.005;
						target_pose6.position.z -=0.005;
						//ROS_INFO("Bajando punto... \n");
					}


					if ((j_==1) && !first) {


						target_pose3.position.z +=0.005;
						target_pose5.position.z +=0.005;

					}

					j_++;

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					full_group.move();
					
					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

				}

				else {

					//Return

					target_pose1.position.x -= d_l * cos(fi_l);
					target_pose1.position.y -= d_l * sin(fi_l);


					target_pose6.position.x -= d_r * cos(fi_r);
					target_pose6.position.y -= d_r * sin(fi_r);


					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					j_=1;
					set1 = false;
					set2 = true;
					set3 = false;

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();


				}

			}

			if (!set1 && set2 && !set3) {


				if (j_ != x_l.size()) {

					target_pose2.position.x += x_r[j_] - x_r[j_-1];
					target_pose2.position.y += y_r[j_] - y_r[j_-1];
					target_pose2.position.z += z_r[j_] - z_r[j_-1];

					target_pose4.position.x += x_l[j_] - x_l[j_-1];
					target_pose4.position.y += y_l[j_] - y_l[j_-1];
					target_pose4.position.z += z_l[j_] - z_l[j_-1];


					if(j_ == (x_l.size()-1)) {
						target_pose2.position.z -=0.005;
						target_pose4.position.z -=0.005;
						//ROS_INFO("Bajando punto... \n");
					}

					if ((j_==1) && !first) {


						target_pose1.position.z +=0.005;
						target_pose6.position.z +=0.005;

					}

					j_++;

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					full_group.move();

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

				}


				else {

					//Return

					target_pose2.position.x -= d_r * cos(fi_r);
					target_pose2.position.y -= d_r * sin(fi_r);


					target_pose4.position.x -= d_l * cos(fi_l);
					target_pose4.position.y -= d_l * sin(fi_l);

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					j_=1;
					set1 = false;
					set2 = false;
					set3 = true;

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

					if (first) {first = false;}


				}
		
			}

			if (!set1 && !set2 && set3){


				if (j_ != x_l.size()) {

					target_pose3.position.x += x_r[j_] - x_r[j_-1];
					target_pose3.position.y += y_r[j_] - y_r[j_-1];
					target_pose3.position.z += z_r[j_] - z_r[j_-1];

					target_pose5.position.x += x_l[j_] - x_l[j_-1];
					target_pose5.position.y += y_l[j_] - y_l[j_-1];
					target_pose5.position.z += z_l[j_] - z_l[j_-1];


					if(j_ == (x_l.size()-1)) {
						target_pose3.position.z -=0.005;
						target_pose5.position.z -=0.005;
						//ROS_INFO("Bajando punto... \n");
					}

					if ((j_==1) && !first) {


						target_pose2.position.z +=0.005;
						target_pose4.position.z +=0.005;

					}

					j_++;

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					full_group.move();

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

				}


				else {

					//Return

					target_pose3.position.x -= d_r * cos(fi_r);
					target_pose3.position.y -= d_r * sin(fi_r);


					target_pose5.position.x -= d_l * cos(fi_l);
					target_pose5.position.y -= d_l * sin(fi_l);

					ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
					ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
					ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
					ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
					ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
					ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

					j_=1;
					set1 = true;
					set2 = false;
					set3 = false;

					v_exec_l = spider.getVLeft();
					v_exec_r = spider.getVRight();
					old_gait_mode = gait_mode;
					old_profile = spider.getProfile();

					if (first) {first = false;}


				}
		
			}
			

		}

    else if (gait_mode == 3) {
    ROS_INFO("HOME EEEEEEEEEEEEEEEEES= %d", home);
      if (home == 1){
        full_group.setJointValueTarget(group_variable_values_full);
        full_group.move();
        max_sweep.data = 0;
      }
      else{
        if (spider.getSweep() > 0.0){//para agacharse
          ROS_INFO("AGACHANDOME");
          if (j_ < 10){
            if (j_ > 0){ //si veníamos de la posición por defecto
              //Patas delanteras: 1 (L1) y 4 (R1)
              ROS_INFO("ME ESTOY AGACHANDO  j_= %d", j_);

              target_pose1.position.x += 0.002886;
              target_pose1.position.y -= 0.002886;
              target_pose1.position.z += 0.005;

              target_pose4.position.x += 0.002886;
              target_pose4.position.y += 0.002886;
              target_pose4.position.z += 0.005;

              //Patas medio: 2 (L2) y 5 (R2)
              /*target_pose2.position.x = 0.0;
              target_pose2.position.y = 0.0;
              target_pose2.position.z = 0.0;

              target_pose5.position.x = 0.0;
              target_pose5.position.y = 0.0;
              target_pose5.position.z = 0.0;*/

              //Patas traseras: 3 (L3) y 6 (R3)
              target_pose3.position.x += 0.002886;
              target_pose3.position.y -= 0.002886;
              target_pose3.position.z -= 0.005;

              target_pose6.position.x += 0.002886;
              target_pose6.position.y += 0.002886;
              target_pose6.position.z -= 0.005;
            }
            else if (j_ < 0){  //si veníamos de estar subidos
              //Patas delanteras: 1 (L1) y 4 (R1)
              target_pose1.position.x += 0.002886;
              target_pose1.position.y += 0.002886;
              target_pose1.position.z += 0.005;

              target_pose4.position.x += 0.002886;
              target_pose4.position.y -= 0.002886;
              target_pose4.position.z += 0.005;

               //Patas medio: 2 (L2) y 5 (R2)
               /*target_pose2.position.x = 0.0;
               target_pose2.position.y = 0.0;
               target_pose2.position.z = 0.0;

               target_pose5.position.x = 0.0;
               target_pose5.position.y = 0.0;
               target_pose5.position.z = 0.0;*/

               //Patas traseras: 3 (L3) y 6 (R3)
               target_pose3.position.x += 0.002886;
               target_pose3.position.y += 0.002886;
               target_pose3.position.z -= 0.005;

               target_pose6.position.x += 0.002886;
               target_pose6.position.y -= 0.002886;
               target_pose6.position.z -= 0.005;
            }

            j_++;

            ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
            ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
            ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
            ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
            ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
            ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

            full_group.move();

            v_exec_l = spider.getVLeft();
            v_exec_r = spider.getVRight();
            old_gait_mode = gait_mode;
            old_profile = spider.getProfile();
            max_sweep.data = 0;
            sweep.publish(max_sweep);
          }
          else { //ya no bajamos más
             ROS_INFO("Spider is looking DOWN... \n");
             max_sweep.data = 1;
             sweep.publish(max_sweep);
          }

        }
        else if (spider.getSweep() < 0.0){//para levantarse
        if (j_ > -10){
          if (j_ > 0){ //si veníamos de la posición de agachado
            //Patas delanteras: 1 (L1) y 4 (R1)
            target_pose1.position.x -= 0.002886;
            target_pose1.position.y += 0.002886;
            target_pose1.position.z -= 0.005;

            target_pose4.position.x -= 0.002886;
            target_pose4.position.y -= 0.002886;
            target_pose4.position.z -= 0.005;

            //Patas medio: 2 (L2) y 5 (R2)
            /*target_pose2.position.x = 0.0;
            target_pose2.position.y = 0.0;
            target_pose2.position.z = 0.0;

            target_pose5.position.x = 0.0;
            target_pose5.position.y = 0.0;
            target_pose5.position.z = 0.0;*/

            //Patas traseras: 3 (L3) y 6 (R3)
            target_pose3.position.x -= 0.002886;
            target_pose3.position.y += 0.002886;
            target_pose3.position.z += 0.005;

            target_pose6.position.x -= 0.002886;
            target_pose6.position.y -= 0.002886;
            target_pose6.position.z += 0.005;

          }
          else if (j_ < 0){ //si veníamos de la posición por defecto
            //Patas delanteras: 1 (L1) y 4 (R1)
            target_pose1.position.x -= 0.002886;
            target_pose1.position.y -= 0.002886;
            target_pose1.position.z -= 0.005;

            target_pose4.position.x -= 0.002886;
            target_pose4.position.y += 0.002886;
            target_pose4.position.z -= 0.005;

            //Patas medio: 2 (L2) y 5 (R2)
            /*target_pose2.position.x = 0.0;
            target_pose2.position.y = 0.0;
            target_pose2.position.z = 0.0;

            target_pose5.position.x = 0.0;
            target_pose5.position.y = 0.0;
            target_pose5.position.z = 0.0;*/

            //Patas traseras: 3 (L3) y 6 (R3)
            target_pose3.position.x -= 0.002886;
            target_pose3.position.y -= 0.002886;
            target_pose3.position.z += 0.005;

            target_pose6.position.x -= 0.002886;
            target_pose6.position.y += 0.002886;
            target_pose6.position.z += 0.005;


          }

          j_--;

          ok_pose1 = full_group.setPositionTarget(target_pose1.position.x,target_pose1.position.y,target_pose1.position.z,"tibia_foot_l1");
          ok_pose2 = full_group.setPositionTarget(target_pose2.position.x,target_pose2.position.y,target_pose2.position.z,"tibia_foot_l2");
          ok_pose3 = full_group.setPositionTarget(target_pose3.position.x,target_pose3.position.y,target_pose3.position.z,"tibia_foot_l3");
          ok_pose4 = full_group.setPositionTarget(target_pose4.position.x,target_pose4.position.y,target_pose4.position.z,"tibia_foot_r1");
          ok_pose5 = full_group.setPositionTarget(target_pose5.position.x,target_pose5.position.y,target_pose5.position.z,"tibia_foot_r2");
          ok_pose6 = full_group.setPositionTarget(target_pose6.position.x,target_pose6.position.y,target_pose6.position.z,"tibia_foot_r3");

          full_group.move();

          v_exec_l = spider.getVLeft();
          v_exec_r = spider.getVRight();
          old_gait_mode = gait_mode;
          old_profile = spider.getProfile();
          max_sweep.data = 0;
          sweep.publish(max_sweep);
        }
        else{ //ya no subimos más

          ROS_INFO("Spider is looking UP... \n");
          max_sweep.data = 1;
          sweep.publish(max_sweep);
        }


      }
      }

    }

  } //final del empty
}   //final del while





ros::shutdown();  
return 0;


} //final del main
