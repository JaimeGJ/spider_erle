#include "spider.hpp"

Spider::Spider(int profile) {

	v_leg = 0.3; //Real~=0.318 m/s
	vx = 0.0;
	vy = 0.0;
	w = 0.0;
	last_w = 0.0;
	d_l = 0.0;
	d_r = 0.0;
	geometry_msgs::Twist initial;
	initial.linear.x = 0.0;
	initial.linear.y = 0.0;
	initial.linear.z = 0.0;

	initial.angular.x = 0.0;
	initial.angular.y = 0.0;
	initial.angular.z = 0.0;

	v_spider = 0.0;
	v_left = 0.0;
	v_right = 0.0;
  sweep = 0.0;

	
	//Set height, max and min velocities

	switch (profile) {


		case 1: 	
			z0 = 0.02;
			max_vel = 0.13;
			min_vel = 0.072;
		break;

		case 2:		
			z0 = 0.05;
			max_vel = 0.1;
			min_vel = 0.03;
				
		break;

		case 3:		
			z0 = 0.08;
			max_vel = 0.095;
			min_vel = 0.024;
		
		break;


		default:	
			z0 = 0.04;
			max_vel = 0.12;
			min_vel = 0.05;
		break;

	}

}


bool Spider::setGait(const geometry_msgs::Twist & command) {


  ROS_INFO("vx: %lf \n vy: %lf \n last_w: %lf \n  Sweep: %lf \n ---------------- \n command x: %lf \n command y: %lf \n command w: %lf \n", vx, vy, last_w, sweep, command.linear.x, command.linear.y, command.angular.x);


	if((vx == command.linear.x) && (vy == command.linear.y) && (last_w == command.angular.x)) {

 
		return false; //Returns if there is not change in joystick command

	}


	vx = command.linear.x;
	vy = command.linear.y;
  sweep = command.linear.z;

	w = command.angular.x;

	last_w = command.angular.x;


	//Sets fi angle and linear velocity

	if(vx == 0.0) { 

		if(vy > 0) {

			fi_l = PI/2;
			fi_r = PI/2;
			}

		else {

			fi_l = -PI/2;
			fi_r = -PI/2;


		}

		if (sqrt(pow(vx,2) + pow(vy,2)) >= 0.1) {

			v_spider = min_vel + ((max_vel - min_vel)/0.9) * (sqrt(pow(vx,2) + pow(vy,2))-0.1);
		}

		else {v_spider = 0.0;}




			}
 
	else {

		fi_l = atan2(vy,vx);
		fi_r = fi_l;

		if (sqrt(pow(vx,2) + pow(vy,2)) >= 0.1) {

			v_spider = min_vel + ((max_vel - min_vel)/0.9) * (sqrt(pow(vx,2) + pow(vy,2))-0.1);
		}

		else {v_spider = 0.0;}

	}


	ROS_INFO("V_SPIDER: %lf \n", v_spider);


	//Set max rotation


	if(v_spider != 0.0) {

		rot_max = (max_vel - v_spider) / v_spider;
		rot_min = -rot_max;

	}

	else {
		
		rot_max = 1.0;
		rot_min = -1.0;
		

	}

	//Set velocity for each one of the sets


	if(v_spider == 0.0) {
	
		rot = w;

		v_left = (-max_vel * rot) / 2;
		v_right = (max_vel * rot) / 2;
		

		if (v_left < 0) {

			fi_l = PI;
			v_left = -v_left;
		}

		else {

			fi_l = 0;
		}


		if (v_right < 0) {

			fi_r = PI;
			v_right = -v_right;
		}

		else {

			fi_r = 0;
		}	


		ROS_INFO("VLEFT: %lf \n", v_left);
		ROS_INFO("VRIGHT: %lf \n",v_right);

	}


	else { 

		if (w > rot_max)

			w = rot_max;

		else if (w < rot_min)
			
			w = rot_min;


		rot = w;

		v_left = (v_spider - v_spider * rot) / 2;
		v_right = (v_spider + v_spider * rot) / 2;

		ROS_INFO("!!VLEFT: %lf \n", v_left);
		ROS_INFO("!!VRIGHT: %lf \n",v_right);


		if (2 * v_left > max_vel)
			
			v_left = max_vel;

		else if (2 * v_left < min_vel)
			
			v_left = min_vel;

		if (2 * v_right > max_vel)
			
			v_right = max_vel;

		else if (2 * v_right < min_vel)
			
			v_right = min_vel;

	}

	//ROS_FATAL("CHANGE!! -------------- \n");
	ROS_INFO("VLEFT: %lf \n", v_left);
	ROS_INFO("VRIGHT: %lf \n",v_right);

	return true;

}

void Spider::setPoints() {

	//Set points

	x_l.clear();
	y_l.clear();
	z_l.clear();

	x_r.clear();
	y_r.clear();
	z_r.clear();

	r_l.clear();
	r_r.clear();

	int i = 0;
	double t = 0;

	//Set w
	double w_r;
	double w_l;


	if (d_r > 0.0)

		w_r = PI / d_r;

	else 
		w_r = 0.0;


	if (d_l > 0.0)

		w_l = PI / d_l;

	else 
		w_l = 0.0;


	//Intervals: we will take 5 points
	double interval_r = d_r / (4 * v_leg);
	double interval_l = d_l / (4 * v_leg);

	//Right set points

	for (t=0 ;  t < d_r / (2 * v_leg) ; t+=interval_r) {

		r_r.push_back(v_leg * t);
		z_r.push_back(z0 * sin (w_r * v_leg * t));

		x_r.push_back(v_leg * t * cos(fi_r));
		y_r.push_back(v_leg * t * sin(fi_r));

	}


	for (t ;  t < d_r / v_leg ; t+=interval_r) {

		r_r.push_back(v_leg * t);
		z_r.push_back(z0 * sin (w_r * v_leg * t));

		x_r.push_back(v_leg * t * cos(fi_r));
		y_r.push_back(v_leg * t * sin(fi_r));

	}

		t = d_r / v_leg;

		r_r.push_back(v_leg * t);
		z_r.push_back(z0 * sin (w_r * v_leg * t));

		x_r.push_back(v_leg * t * cos(fi_r));
		y_r.push_back(v_leg * t * sin(fi_r));


	//Left set points


	for (t=0 ;  t < d_l / (2 * v_leg) ; t+=interval_l) {

		r_l.push_back(v_leg * t);
		z_l.push_back(z0 * sin (w_l * v_leg * t));

		x_l.push_back(v_leg * t * cos(fi_l));
		y_l.push_back(v_leg * t * sin(fi_l));

	}


	for (t ;  t < d_l / v_leg ; t+=interval_l) {

		r_l.push_back(v_leg * t);
		z_l.push_back(z0 * sin (w_l * v_leg * t));

		x_l.push_back(v_leg * t * cos(fi_l));
		y_l.push_back(v_leg * t * sin(fi_l));

	}

		t = d_l / v_leg;

		r_l.push_back(v_leg * t);
		z_l.push_back(z0 * sin (w_l * v_leg * t));

		x_l.push_back(v_leg * t * cos(fi_l));
		y_l.push_back(v_leg * t * sin(fi_l));


			/*for (i = 0; i < x_l.size(); i++) {

			ROS_INFO("xi: %f \n yi: %f \n zi: %f \n", x_l[i], y_l[i], z_l[i]);

			}

			for (i = 0; i < x_l.size(); i++) {

			ROS_INFO("xd: %f \n yd: %f \n zd: %f \n", x_r[i], y_r[i], z_r[i]);
			}*/


}

void Spider::setProfile(int p) {


	switch(p) {

		case 1: 
			z0 = 0.02;
			max_vel = 0.13;
			min_vel = 0.072;
		break;

		case 2:
			z0 = 0.05;
			max_vel = 0.1;
			min_vel = 0.03;
		break;

		case 3:
			z0 = 0.08;
			max_vel = 0.095;
			min_vel = 0.024;
		break;

		default:
			z0 = 0.04;
			max_vel = 0.12;
			min_vel = 0.05;
		break;
	}

}

int Spider::getProfile() {


	if (z0 == 0.02)
		return 1;
	else if (z0 == 0.05)
		return 2;
	else if (z0 == 0.08)
		return 3;
	else
		return 0;


}






