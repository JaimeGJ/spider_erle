
#ifndef CALCULATE_TRAJECTORIES_HPP
#define CALCULATE_TRAJECTORIES_HPP

#include <math.h>
#include <gsl/gsl_integration.h>

#define PI 3.14159265359

//Arcs

double f_low (double t, void * params);
double f_medium (double t, void * params);
double f_medium (double t, void * params);

void calculateTrajectory(double v_spider, double z0, double * d, double v_leg);


#endif
