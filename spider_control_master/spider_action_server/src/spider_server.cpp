//http://clopema.felk.cvut.cz/redmine/projects/clopema/wiki/Sending_trajectory_to_the_controller
//http://wiki.ros.org/actionlib_tutorials/Tutorials/SimpleActionServer%28ExecuteCallbackMethod%29
//http://wiki.ros.org/actionlib_tutorials/Tutorials/SimpleActionServer%28GoalCallbackMethod%29
#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <control_msgs/FollowJointTrajectoryAction.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <crab_msgs/LegsJointsState.h>
#include <crab_msgs/GaitCommand.h>
#include <sensor_msgs/JointState.h>

class RobotTrajectoryFollower
{
protected:

  ros::NodeHandle nh_;
  // NodeHandle instance must be created before this line. Otherwise strange error may occur.
  actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> as_; 
  std::string action_name_;
  trajectory_msgs::JointTrajectory goal_;
  control_msgs::FollowJointTrajectoryResult result_;
  control_msgs::FollowJointTrajectoryFeedback feedback_;
  ros::Subscriber sub_pose = nh_.subscribe("/spider/joint_states", 1, &RobotTrajectoryFollower::executeCB, this);
  ros::Publisher pub_pose = nh_.advertise<crab_msgs::LegsJointsState>("/joints_to_controller", 1000,this);
  ros::Publisher pub_gait_control = nh_.advertise<crab_msgs::GaitCommand>("/teleop/gait_control",1000, this);
  

  int n_leg;
  unsigned long int k , n_points;
  bool flag;

public:

  RobotTrajectoryFollower(std::string name, int leg) :
    as_(nh_, name, false),
    action_name_(name)
  {
    //Register callback functions:
    as_.registerGoalCallback(boost::bind(&RobotTrajectoryFollower::goalCB, this));
    as_.registerPreemptCallback(boost::bind(&RobotTrajectoryFollower::preemptCB, this));
    n_leg = leg;
    k = 0;
    n_points = 0;
    flag = false;

    as_.start();
  }

  ~RobotTrajectoryFollower(void)//Destructor
  {
  }

  void goalCB()
  {
    // accept the new goal
    goal_ = as_.acceptNewGoal()->trajectory;
    
    if(!flag) {	
    	n_points = goal_.points.size();
	flag=true;
    }
 
  }

  void preemptCB()
  {
    ROS_INFO("%s: Preempted", action_name_.c_str());
    // set the action state to preempted
    as_.setPreempted();
  }

  ros::NodeHandle getNode() {return nh_;}

  void executeCB(const sensor_msgs::JointState& data) {

	crab_msgs::LegsJointsState Legs_state;
	crab_msgs::LegJointsState leg[6];
	crab_msgs::LegJointsState leg_aux[6];
	crab_msgs::GaitCommand gait_command;
	trajectory_msgs::JointTrajectory trajectory;
	int i = 0,j = 0;

	ROS_INFO("Tamano de la trayectoria: %lu", n_points);
	
	if (!as_.isActive())
	return;
	
	ros::Time now = ros::Time::now(); 
	gait_command.cmd = gait_command.MOVEITCOMMAND;
	
	//Feedback
	//Tenemos que publicar el feedback => transformar JointState a JointTrajectory
	feedback_.header.stamp = now;
	
	feedback_.joint_names.resize(3);
	feedback_.actual.positions.resize(3);
	feedback_.actual.velocities.resize(3);
	feedback_.actual.effort.resize(3);
	

	for (i=0; i<3;i++) {
		
		feedback_.joint_names[i] = data.name[i + 3 * n_leg];
		feedback_.actual.positions[i] = data.position [i + 3 * n_leg];
		feedback_.actual.velocities[i] = data.velocity [i + 3 * n_leg];
		feedback_.actual.effort[i] = data.effort [i + 3 * n_leg];
	}
	
	//Goal

	trajectory = goal_;
	trajectory_msgs::JointTrajectoryPoint actual_point;

	actual_point.positions.resize(3);
	actual_point.velocities.resize(3);
	actual_point.accelerations.resize(3);
	actual_point.effort.resize(3);


	if(k != n_points) {
	
	ROS_INFO("Obtener punto a realizar:");
	actual_point = trajectory.points[k];



	for(i=0; i<6; i++) {
		for(j=0; j<3; j++) {
		
			//leg = Legs_state.joints_state[i];

			if(i == n_leg) {

				leg[i].joint[j] = actual_point.positions[j];
				leg[i].velocity[j] = actual_point.velocities[j];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);
			}

			else {
				if(k == 0) {

					//Mantener posición
					leg_aux[i].joint[j] = data.position[3 * i + j];
					leg_aux[i].velocity[j] = data.velocity[3 * i + j];
					//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);
				}

				leg[i].joint[j] = leg_aux[i].joint[j];
				leg[i].velocity[j] = leg_aux[i].velocity[j];
			}
		}

	}
	

	for(i=0;i<6;i++)

		Legs_state.joints_state[i] = leg[i];



	ROS_INFO("Publicar joint_states");
	pub_gait_control.publish(gait_command);
	pub_pose.publish(Legs_state);

	feedback_.desired = actual_point;
	feedback_.desired.positions.resize(4);
	feedback_.error.positions.resize(4);

	for(i=0;i<3;i++) {

		feedback_.error.positions[i] = feedback_.desired.positions[i] - feedback_.actual.positions[i];
	}

		//Publish feedback

		as_.publishFeedback(feedback_);

		k++;
	} 

	else { 

	
	result_.error_code = 0;
	ROS_INFO("%s: Succeeded", action_name_.c_str());
	// set the action state to succeeded
	as_.setSucceeded(result_);
	k = 0;
	flag=false;



	//Get linear velocities
	


	}


}
  
};


class RobotMultipleTrajectoryFollower
{
protected:

  ros::NodeHandle nh_;
  // NodeHandle instance must be created before this line. Otherwise strange error may occur.
  actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> as_; 
  std::string action_name_;
  trajectory_msgs::JointTrajectory goal_;
  control_msgs::FollowJointTrajectoryResult result_;
  control_msgs::FollowJointTrajectoryFeedback feedback_;
  ros::Subscriber sub_pose = nh_.subscribe("/joint_states", 1, &RobotMultipleTrajectoryFollower::executeCB, this);
  ros::Publisher pub_pose = nh_.advertise<crab_msgs::LegsJointsState>("/joints_to_controller", 1000,this);
  ros::Publisher pub_gait_control = nh_.advertise<crab_msgs::GaitCommand>("/teleop/gait_control",1000, this);
  

  int n_set;
  unsigned long int k , n_points;
  bool flag;

public:

  RobotMultipleTrajectoryFollower(std::string name, int set) :
    as_(nh_, name, false),
    action_name_(name)
  {
    //Register callback functions:
    as_.registerGoalCallback(boost::bind(&RobotMultipleTrajectoryFollower::goalCB, this));
    as_.registerPreemptCallback(boost::bind(&RobotMultipleTrajectoryFollower::preemptCB, this));
    n_set = set;
    k = 0;
    n_points = 0;
    flag = false;

    as_.start();
  }

  ~RobotMultipleTrajectoryFollower(void)//Destructor
  {
  }

  void goalCB()
  {
    // accept the new goal
    goal_ = as_.acceptNewGoal()->trajectory;
    
    if(!flag) {	
    	n_points = goal_.points.size();
	flag=true;
    }
 
  }

  void preemptCB()
  {
    ROS_INFO("%s: Preempted", action_name_.c_str());
    // set the action state to preempted
    as_.setPreempted();
  }

  ros::NodeHandle getNode() {return nh_;}

  void executeCB(const sensor_msgs::JointState& data) {

	crab_msgs::LegsJointsState Legs_state;
	crab_msgs::LegJointsState leg[6];
	crab_msgs::GaitCommand gait_command;
	trajectory_msgs::JointTrajectory trajectory;
	int i = 0,j = 0;

	ROS_INFO("Tamano de la trayectoria: %lu", n_points);
	
	if (!as_.isActive())
	return;
	
	ros::Time now = ros::Time::now(); 
	gait_command.cmd = gait_command.MOVEITCOMMAND;
	
	//Feedback
	//Tenemos que publicar el feedback => transformar JointState a JointTrajectory
	feedback_.header.stamp = now;
	
	feedback_.joint_names.resize(9);
	feedback_.actual.positions.resize(9);
	feedback_.actual.velocities.resize(9);
	feedback_.actual.effort.resize(9);
	

	for (i=0; i<9;i++) {
		
		feedback_.joint_names[i] = data.name[2 * i + n_set];
		feedback_.actual.positions[i] = data.position [2 * i + n_set];
		feedback_.actual.velocities[i] = data.velocity [2 * i + n_set];
		feedback_.actual.effort[i] = data.effort [2 * i + n_set];
	}
	
	//Goal

	trajectory = goal_;
	trajectory_msgs::JointTrajectoryPoint actual_point;

	actual_point.positions.resize(9);
	actual_point.velocities.resize(9);
	actual_point.accelerations.resize(9);
	actual_point.effort.resize(9);	

	if(k != n_points) {
	
	ROS_INFO("Obtener punto a realizar:");
	actual_point = trajectory.points[k];


	switch (n_set) {

	case 0 : 
		
		for(i=0; i<6; i++) {

			for(j=0; j<3; j++) {

				if (i == 3) {

				leg[i].joint[j] = actual_point.positions[3 * j];
				leg[i].velocity[j] = actual_point.velocities[3 * j];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_l1

				else if (i == 5) {

				leg[i].joint[j] = actual_point.positions[3 * j + 1];
				leg[i].velocity[j] = actual_point.velocities[3 * j + 1];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_l3

				else if (i == 1) {

				leg[i].joint[j] = actual_point.positions[3 * j + 2];
				leg[i].velocity[j] = actual_point.velocities[3 * j + 2];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_r2

				else {
				//Mantener posición
				leg[i].joint[j] = data.position[3 * i + j];
				leg[i].velocity[j] = data.velocity[3 * i + j];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);
				}		

			}

		}

		break;


	case 1 :

		for(i=0; i<6; i++) {

			for(j=0; j<3; j++) {

				if (i == 0) {

				leg[i].joint[j] = actual_point.positions[3 * j + 1];
				leg[i].velocity[j] = actual_point.velocities[3 * j + 1];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_r1

				else if (i == 2) {

				leg[i].joint[j] = actual_point.positions[3 * j + 2];
				leg[i].velocity[j] = actual_point.velocities[3 * j + 2];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_r3

				else if (i == 4) {

				leg[i].joint[j] = actual_point.positions[3 * j];
				leg[i].velocity[j] = actual_point.velocities[3 * j];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);

				} //leg_l2

				else {
				//Mantener posición
				leg[i].joint[j] = data.position[3 * i + j];
				leg[i].velocity[j] = data.velocity[3 * i + j];
				//ROS_INFO("leg.joint[%d][%d]: %lf", i, j, leg[i].joint[j]);
				}		

			}

		}

		break;

	default:
		break;

	}
	

	for(i=0;i<6;i++)

		Legs_state.joints_state[i] = leg[i];



	ROS_INFO("Publicar joint_states");
	pub_gait_control.publish(gait_command);
	pub_pose.publish(Legs_state);

	feedback_.desired = actual_point;
	feedback_.desired.positions.resize(9);
	feedback_.error.positions.resize(9);

	for(i=0;i<9;i++) {

		feedback_.error.positions[i] = feedback_.desired.positions[i] - feedback_.actual.positions[i];
	}

		//Publish feedback

		as_.publishFeedback(feedback_);

		k++;
	} 

	else { 

	
	result_.error_code = 0;
	ROS_INFO("%s: Succeeded", action_name_.c_str());
	// set the action state to succeeded
	as_.setSucceeded(result_);
	k = 0;
	flag=false;


	}
}
  
};

class RobotAllTrajectoryFollower
{
protected:

  ros::NodeHandle nh_;
  // NodeHandle instance must be created before this line. Otherwise strange error may occur.
  actionlib::SimpleActionServer<control_msgs::FollowJointTrajectoryAction> as_; 
  std::string action_name_;
  trajectory_msgs::JointTrajectory goal_;
  control_msgs::FollowJointTrajectoryResult result_;
  control_msgs::FollowJointTrajectoryFeedback feedback_;
  ros::Subscriber sub_pose = nh_.subscribe("/spider/joint_states", 1, &RobotAllTrajectoryFollower::executeCB, this);
  ros::Publisher pub_pose = nh_.advertise<crab_msgs::LegsJointsState>("/joints_to_controller", 1,this);
  ros::Publisher pub_gait_control = nh_.advertise<crab_msgs::GaitCommand>("/teleop/gait_control",1, this);
  

  unsigned long int k , n_points;
  bool flag;

public:

  RobotAllTrajectoryFollower(std::string name) :
    as_(nh_, name, false),
    action_name_(name)
  {
    //Register callback functions:
    as_.registerGoalCallback(boost::bind(&RobotAllTrajectoryFollower::goalCB, this));
    as_.registerPreemptCallback(boost::bind(&RobotAllTrajectoryFollower::preemptCB, this));
    k = 0;
    n_points = 0;
    flag = false;

    as_.start();
  }

  ~RobotAllTrajectoryFollower(void)//Destructor
  {
  }

  void goalCB()
  {
    // accept the new goal
    goal_ = as_.acceptNewGoal()->trajectory;
    
    if(!flag) {	
    	n_points = goal_.points.size();
	flag=true;
    }
 
  }

  void preemptCB()
  {
    ROS_INFO("%s: Preempted", action_name_.c_str());
    // set the action state to preempted
    as_.setPreempted();
  }

  ros::NodeHandle getNode() {return nh_;}

  void executeCB(const sensor_msgs::JointState& data) {

	crab_msgs::LegsJointsState Legs_state;
	crab_msgs::LegJointsState leg[6];
	crab_msgs::GaitCommand gait_command;
	trajectory_msgs::JointTrajectory trajectory;
	int i = 0,j = 0;

	ROS_INFO("Tamano de la trayectoria: %lu", n_points);
	
	if (!as_.isActive())
	return;
	
	ros::Time now = ros::Time::now(); 
	gait_command.cmd = gait_command.MOVEITCOMMAND;
	
	//Feedback
	//Tenemos que publicar el feedback => transformar JointState a JointTrajectory
	feedback_.header.stamp = now;
	
	feedback_.joint_names.resize(18);
	feedback_.actual.positions.resize(18);
	feedback_.actual.velocities.resize(18);
	feedback_.actual.effort.resize(18);
	

	for (i=0; i<18;i++) {
		
		feedback_.joint_names[i] = data.name[i];
		feedback_.actual.positions[i] = data.position [i];
		feedback_.actual.velocities[i] = data.velocity [i];
		feedback_.actual.effort[i] = data.effort [i];
	}
	
	//Goal

	trajectory = goal_;
	trajectory_msgs::JointTrajectoryPoint actual_point;

	actual_point.positions.resize(18);
	actual_point.velocities.resize(18);
	actual_point.accelerations.resize(18);
	actual_point.effort.resize(18);	

	if(k != n_points) {
	
	ROS_INFO("Obtener punto a realizar:");
	actual_point = trajectory.points[k];


	for(i=0;i<6;i++) {

		for(j = 0; j<3; j++) {


				if(i < 3) {				

					leg[3 + i].joint[j] = actual_point.positions[i + 6 * j];
					leg[3 + i].velocity[j] = actual_point.velocities[i + 6 * j];

				}

				else {
					leg[i - 3].joint[j] = actual_point.positions[i + 6 * j];
					leg[i - 3].velocity[j] = actual_point.velocities[i + 6 * j];
				}

			}
	}
	

	for(i=0;i<6;i++)

		Legs_state.joints_state[i] = leg[i];



	ROS_INFO("Publicar joint_states");
	pub_gait_control.publish(gait_command);
	pub_pose.publish(Legs_state);

	feedback_.desired = actual_point;
	feedback_.desired.positions.resize(18);
	feedback_.error.positions.resize(18);

	for(i=0;i<18;i++) {

		feedback_.error.positions[i] = feedback_.desired.positions[i] - feedback_.actual.positions[i];
	}

		//Publish feedback

		as_.publishFeedback(feedback_);

		k++;
	} 

	else { 

	
	result_.error_code = 0;
	ROS_INFO("%s: Succeeded", action_name_.c_str());
	// set the action state to succeeded
	as_.setSucceeded(result_);
	k = 0;
	flag=false;


	}
}
  
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "action_server");

  RobotTrajectoryFollower RobotTrajectoryFollower_l1("/leg_l1_controller/joint_trajectory_action",3);
  RobotTrajectoryFollower RobotTrajectoryFollower_l2("/leg_l2_controller/joint_trajectory_action",4);
  RobotTrajectoryFollower RobotTrajectoryFollower_l3("/leg_l3_controller/joint_trajectory_action",5);
  RobotTrajectoryFollower RobotTrajectoryFollower_r1("/leg_r1_controller/joint_trajectory_action",0);
  RobotTrajectoryFollower RobotTrajectoryFollower_r2("/leg_r2_controller/joint_trajectory_action",1);
  RobotTrajectoryFollower RobotTrajectoryFollower_r3("/leg_r3_controller/joint_trajectory_action",2);
  RobotMultipleTrajectoryFollower RobotMultipleTrajectoryFollower_leg_set_1("/leg_set_1_controller/joint_trajectory_action",0);
  RobotMultipleTrajectoryFollower RobotMultipleTrajectoryFollower_leg_set_2("/leg_set_2_controller/joint_trajectory_action",1);
  RobotAllTrajectoryFollower RobotAllTrajectoryFollower_all_legs("/all_legs_controller/joint_trajectory_action");

  ros::spin();

  return 0;
}
