#include <ros/ros.h>
#include <ros/console.h>
#include <geometry_msgs/Twist.h>
#include <actionlib_msgs/GoalStatusArray.h>
#include <actionlib_msgs/GoalStatus.h>
#include <math.h>
#include <iostream>
#include <unistd.h>


geometry_msgs::Twist vel;
geometry_msgs::Twist old_vel;
float sum_vel = 0;
int goal_status = 0;
bool publish_old = false;


void velCallback(const geometry_msgs::Twist::ConstPtr& msg){

  vel.linear.x = msg->linear.x;
  vel.linear.y = msg->linear.y;
  vel.linear.z = msg->linear.z;
  vel.angular.x = msg->angular.z;
  vel.angular.y = msg->angular.z;
  vel.angular.z = msg->angular.z;
  sum_vel = msg->linear.x + msg->linear.y + msg->linear.z + msg->angular.z;
  if (sum_vel == 0.0){
    ROS_INFO("0s detected, publishing old velocities");
    if (publish_old){
    vel.linear.x = old_vel.linear.x;
    vel.linear.y = old_vel.linear.y;
    vel.linear.z = old_vel.linear.z;
    vel.angular.x = old_vel.angular.x;
    vel.angular.y = old_vel.angular.y;
    vel.angular.z = old_vel.angular.z;
    }
  }
  else {
    old_vel.linear.x = msg->linear.x;
    old_vel.linear.y = msg->linear.y;
    old_vel.linear.z = msg->linear.z;
    old_vel.angular.x = msg->angular.z;
    old_vel.angular.y = msg->angular.z;
    old_vel.angular.z = msg->angular.z;
  }


}



void goalCallback(const actionlib_msgs::GoalStatusArray::ConstPtr& msg){


    if (!msg->status_list.empty()){
      actionlib_msgs::GoalStatusArray status_array = *msg;
      //uint32_t sequence = status_array.header.seq;
      // sequence is read successfully

      actionlib_msgs::GoalStatus status_list_entry = status_array.status_list[0];
      // status_list_entry is read unsuccessfully. The above line causes a seg fault at runtime.
      ROS_INFO("status_list[0] = %d ", status_list_entry.status);
      if ((status_list_entry.status != 3) && status_list_entry.status != 4){
        publish_old = true;
        ROS_INFO("Goal not reached, publishing all except 0s");
      }
      else {
        publish_old = false;
        ROS_INFO("goal reached, can publish now 0s");
      }
    }

}

int main (int argc, char ** argv){

  ros::init(argc, argv, "angular_remap");
  ros::NodeHandle n;

  ros::Subscriber vel_sub = n.subscribe<geometry_msgs::Twist>("/cmd_vel", 1, velCallback);
  ros::Subscriber goal_sub = n.subscribe<actionlib_msgs::GoalStatusArray>("/move_base/status", 10, goalCallback);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("/pad_teleop/cmd_vel", 10);

  ros::Rate r(10.0);


  while( ros::ok() )
  {
  ros::spinOnce();
  twist_pub.publish(vel);
  r.sleep();
  }
}


/*- if(!goal_reach)

       spider -> move_with_velocity(velocity)

   - if(velocity == 0 && !goal_reach)

       spider -> move_with_velocity(velocity_old)
       */


/*GoalID goal_id
uint8 status
uint8 PENDING         = 0   # The goal has yet to be processed by the action server
uint8 ACTIVE          = 1   # The goal is currently being processed by the action server
uint8 PREEMPTED       = 2   # The goal received a cancel request after it started executing
                            #   and has since completed its execution (Terminal State)
uint8 SUCCEEDED       = 3   # The goal was achieved successfully by the action server (Terminal State)
uint8 ABORTED         = 4   # The goal was aborted during execution by the action server due
                            #    to some failure (Terminal State)
uint8 REJECTED        = 5   # The goal was rejected by the action server without being processed,
                            #    because the goal was unattainable or invalid (Terminal State)
uint8 PREEMPTING      = 6   # The goal received a cancel request after it started executing
                            #    and has not yet completed execution
uint8 RECALLING       = 7   # The goal received a cancel request before it started executing,
                            #    but the action server has not yet confirmed that the goal is canceled
uint8 RECALLED        = 8   # The goal received a cancel request before it started executing
                            #    and was successfully cancelled (Terminal State)
uint8 LOST            = 9   # An action client can determine that a goal is LOST. This should not be
                            #    sent over the wire by an action server

#Allow for the user to associate a string with GoalStatus for debugging
string text*/
