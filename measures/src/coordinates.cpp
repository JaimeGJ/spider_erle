#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl/point_types.h>
#include <boost/foreach.hpp>
#include <std_msgs/Float32.h>
#include <std_msgs/Int8.h>
#include <geometry_msgs/PolygonStamped.h>
#include <sensor_msgs/LaserScan.h>

#include <diagnostic_updater/diagnostic_updater.h>
#include <diagnostic_updater/publisher.h>
#include <log4cxx/logger.h>

float anterior = 0;
float actual = 0;
float max_y = 0;
float min_y = 50;
float last_z = 0;

float min_laser = 10.0;
float max_laser = 0.0;

float altura = 0;

typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

void laserCallback(const sensor_msgs::LaserScan::ConstPtr& msg){
	    min_laser = 10.0;
        max_laser = 0.0;
		for (int ii=0; ii<=(msg->angle_max - msg->angle_min)/msg->angle_increment; ii++){
			if (msg->ranges[ii] <= min_laser){
				min_laser = msg->ranges[ii];
			}
			if (msg->ranges[ii] >= max_laser){
				max_laser = msg->ranges[ii];
			}
		}
		//ROS_INFO("Datos del laser tomados: max = %0.3f || min = %0.3f", max_laser, min_laser);
		//factor = (max_laser - min_laser)/255;
		//ROS_INFO("Factor de conversion: %0.3f", factor);
}

void CloudCallback(const PointCloud::ConstPtr& msg)
{
  //ros::NodeHandle n;
  //ros::Publisher pub = n.advertise<std_msgs::Float32>("/altura_objeto", 1);
  //printf ("Cloud: width = %d, height = %d\n", msg->width, msg->height);
  BOOST_FOREACH (const pcl::PointXYZ& pt, msg->points)
  if (pt.z <= (min_laser+0.03)){
    if(float(fabs(pt.z - last_z)) <= 0.03){
		//ROS_INFO("Distancia al objeto DISTINTA a la anterior");
		  if (pt.y >= max_y){
			  max_y = pt.y;
		  }
		  if (pt.y <= min_y){
			  min_y = pt.y;
		  }
		  //printf ("(X: %f, Y: %f, Z: %f)\n", pt.x, pt.y, pt.z);
		  actual = max_y - min_y;
		  if(actual != anterior){
        anterior = actual;
        ROS_INFO("Altura del objeto actual: %0.3f", actual);
        ROS_INFO("Perfil de altura: %f", altura);
        if (actual <= altura){
          ROS_DEBUG("Se puede pasar por encima del obstaculo");
        }
        else {
          if(altura > 0.1){
            ROS_ERROR("No se puede pasar por encima de ese obstaculo");
          }
          else{
            ROS_WARN("Altura de las patas insuficiente. Es necesario incrementarla");
          }
        }
		  }
	  }
	  else{
		  //ROS_INFO("Distancia al objeto IGUAL a la anterior");  
		  //min_z = pt.z; 
		  last_z = pt.z;
		  max_y = 0;
          min_y = 50;
          anterior = 0;
		  actual = 0;
      }
  
  //ros::Duration(0.5).sleep(); // sleep for half a second
  }
}

void alturaCallback(const std_msgs::Int8::ConstPtr& msg)
{
  switch(msg->data){
    case 1:
      altura = 0.05;
      break;
    case 2:
      altura = 0.08;
      break;
    case 3:
      altura = 0.12;
      break;
  }

  //altura = msg->data;

}




int main(int argc, char** argv)
{

  ROSCONSOLE_AUTOINIT;
  log4cxx::LoggerPtr my_logger = log4cxx::Logger::getLogger(ROSCONSOLE_DEFAULT_NAME);
  // Set the logger for this package to output all statements
  my_logger->setLevel(ros::console::g_level_lookup[ros::console::levels::Debug]);

  ros::init(argc, argv, "sub_pcl");
  ros::NodeHandle nh;
  ros::Subscriber sub_cloud = nh.subscribe<PointCloud>("/voxel_grid/output", 1, CloudCallback);
  ros::Subscriber sub_laser = nh.subscribe("/scan", 1, laserCallback);
  ros::Subscriber sub_altura = nh.subscribe("/pad_teleop/height", 1, alturaCallback);
  //ros::Subscriber sub_polygon = nh.subscribe<geometry_msgs::PolygonStamped>("/convex_hull/output_polygon", 1, PolygonCallback);
  //ros::AsyncSpinner spinner(4); // Use 4 threads
  //spinner.start();
  ros::spin();
}
