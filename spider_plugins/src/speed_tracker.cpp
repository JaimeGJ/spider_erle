// ROS 
#include <ros/ros.h>

// Custom Callback Queue
#include <ros/callback_queue.h>
#include <ros/advertise_options.h>

// Boost
#include <boost/thread.hpp>
#include <boost/bind.hpp>

//Subscriber crab_maestro_controller
#include <crab_msgs/LegsJointsState.h>
#include <crab_msgs/GaitCommand.h>

#include <math.h>




void speedCallback(const crab_msgs::LegJointsState data) {


	ROS_INFO("Velocity:\n x: %lf \n y: %lf \n z: %lf \n", data.joint[0], data.joint[1], data.joint[2]);

}


int main(int argc, char **argv)
{

  ros::init(argc, argv, "speed_publisher");

  ros::NodeHandle n;

  ros::Subscriber sub_1 = n.subscribe<crab_msgs::LegJointsState>("/gazebo", 1000, speedCallback);

  ros::spin();

  return 0;

}
